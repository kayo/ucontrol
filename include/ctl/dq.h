#ifndef __CTL__DQ_H__
#define __CTL__DQ_H__
/**
 * @ingroup converters
 * @defgroup dq DQ coordinate transformations
 *
 * @brief This module implements direct-quadrature-zero transformations (also known as Park transformations).
 * @{
 */

#include <ctl/type.h>

/**
 * @brief Convert stationary αβ coordinates to rotating DQ coordinates
 *
 * The direct Park transformation
 *
 * @param[in] type The scalar type to operate
 * @param[in] ab The pointer to stationary @p αβ vector values
 * @param[in] phy The rotating angle value
 * @param[out] dqr The pointer to rotating @p DQ vector values
 */
#define ab_to_dq(type, ab, phy, dq) {               \
    type sin_phy = gen_sin(type, (phy));            \
    type cos_phy = gen_cos(type, (phy));            \
                                                    \
    /* d = α * cos(phy) + β * sin(phy) */           \
    (dq)->d = gen_add(type,                         \
                      gen_mul(type,                 \
                              (ab)->a, cos_phy),    \
                      gen_mul(type,                 \
                              (ab)->b, sin_phy));   \
                                                    \
    /* q = β * cos(phy) - α * sin(phy) */           \
    (dq)->q = gen_sub(type,                         \
                      gen_mul(type,                 \
                              (ab)->b, cos_phy),    \
                      gen_mul(type,                 \
                              (ab)->a, sin_phy));   \
  }

/**
 * @brief Convert rotating DQ coordinates to stationary αβ coordinates
 *
 * The inverted Park transformation
 *
 * @param[in] type The scalar type to operate
 * @param[in] dq The pointer to rotating @p DQ vector values
 * @param[in] phy The rotating angle value
 * @param[out] ab The pointer to stationary @p αβ vector values
 */
#define dq_to_ab(type, dq, phy, ab) {               \
    type sin_phy = gen_sin(type, (phy));            \
    type cos_phy = gen_cos(type, (phy));            \
                                                    \
    /* α = d * cos(phy) - q * sin(phy) */           \
    (ab)->a = gen_sub(type,                         \
                      gen_mul(type,                 \
                              (dq)->d, cos_phy),    \
                      gen_mul(type,                 \
                              (dq)->q, sin_phy));   \
                                                    \
    /* β = q * cos(phy) + d * sin(phy) */           \
    (ab)->b = gen_add(type,                         \
                      gen_mul(type,                 \
                              (dq)->q, cos_phy),    \
                      gen_mul(type,                 \
                              (dq)->d, sin_phy));   \
  }

/**
 * @}
 */
#endif /* __CTL__DQ_H__ */

#ifndef __CTL__FIR_H__
#define __CTL__FIR_H__
/**
 * @ingroup filters
 * @defgroup fir FIR filter
 * @brief This part implements finite impulse response (FIR) filtering.
 *
 * To simplify filter design (weight coefficients calculation) you can use [GNU Autogen](https://www.gnu.org/software/autogen/) template _src/filter.tpl_. See the example of filter definition below:
 *
 * @code{.c}
 *     autogen definitions filter;
 *
 *     filter = {
 *       class = fir; // the class of filter
 *       name = "<class>_<type>_<window>"; // the name of weights array
 *       wrap = "gen_make(<type>, <val>)"; // wrapper for filter weights
 *       static; // declare array as static
 *       const;  // declare array as constant
 *       type = fix16; // the type for weights
 *       order = 7; // the number of value in delay line (filter order/length)
 *       sample = 100, uS; // the sampling frequency (or period)
 *       pass = 1, KHz; // the pass frequency (or period)
 *       cut = 10, Hz; // the cut-off frequency (or period)
 *     };
 * @endcode
 *
 * To define several filters with shared parameters you can use nesting.
 * See _tests/fir.def_ for example.
 *
 * @{
 */

#include <ctl/type.h>
#include <ctl/dl.h>

/**
 * @brief Get filter weights
 *
 * @param[in] fir The pointer to filter params
 * @return The number of filter weights
 */
#define fir_size(fir) \
  (sizeof(fir) /      \
   sizeof((fir)[0]))

/**
 * @brief Evaluate filtering step
 *
 * @param[in] type The value type to operate
 * @param[in] fir The pointer to weights array
 * @param[in] dl The pointer to delay line
 * @return The filtered value
 */
#define fir_step(type, fir, dl) ({            \
      uint8_t idx = 0;                        \
      type acc = gen_0(type);                 \
                                              \
      for (; idx < fir_size(fir); idx ++) {   \
        acc =                                 \
          gen_add(type,                       \
                  acc,                        \
                  gen_mul(type,               \
                          (fir)[idx],         \
                          dl_read(dl, idx))); \
      }                                       \
                                              \
      acc;                                    \
    })

/**
 * @}
 */
#endif /* __CTL__FIR_H__ */

#ifndef __CTL__UTIL_H__
#define __CTL__UTIL_H__
/**
 * @addtogroup utils
 * @brief This part implements some useful utilities.
 *
 * @{
 */

#include <ctl/type.h>

/**
 * @defgroup common Common utils
 * @brief This part implements common utilities.
 *
 * @{
 */

/**
 * @brief Get absolute value
 *
 * @param[in] type The type of values
 * @param[in] value The actual value
 * @return The absolute value
 */
#define abs_val(type, value)           \
  (gen_lt(type, value, gen_0(type)) ?  \
   gen_neg(type, value) :              \
   (value))

/**
 * @brief Estimate actual error using target and actual value
 *
 * @param[in] type The type of values
 * @param[in] target The target value
 * @param[in] actual The actual value
 * @return error The error value
 */
#define error_val(type, target, actual)         \
  gen_sub(type, actual, target)

/**
 * @brief Get minimum value of two values
 *
 * @param[in] type The type of values
 * @param[in] val1 The first value
 * @param[in] val2 The second value
 * @return The result value
 *
 * If required the both values must be prescaled to common range.
 *
 * res = val1 < val2 ? val1 : val2
 */
#define min_val(type, val1, val2)               \
  (gen_lt(type, val1, val2) ? (val1) : (val2))

/**
 * @brief Get maximum value of two values
 *
 * @param[in] type The type of values
 * @param[in] val1 The first value
 * @param[in] val2 The second value
 * @return The result value
 *
 * If required the both values must be prescaled to common range.
 *
 * res = val1 < val2 ? val2 : val1
 */
#define max_val(type, val1, val2)               \
  (gen_lt(type, val1, val2) ? (val2) : (val1))

/**
 * @brief Get middle value between two values
 *
 * @param[in] type The type of values
 * @param[in] val1 The first value
 * @param[in] val2 The second value
 * @return The result value
 *
 * If required the both values must be prescaled to common range.
 *
 * res = (val1 + val2) / 2
 */
#define mid_val(type, val1, val2)       \
  lerp_val_const(type, val1, val2, 0.5)

/**
 * @brief Linear interpolation between two values
 *
 * @param[in] type The type of values
 * @param[in] val1 The first value
 * @param[in] val2 The second value
 * @param[in] fact The interpolation factor [0...1]
 * @return The result value
 *
 * If required the both values must be prescaled to common range.
 *
 * res = val1 * (1 - fact) + val2 * fact
 */
#define lerp_val(type, val1, val2, fact)             \
  gen_add(type,                                      \
          gen_mul(type, val1,                        \
                  gen_sub(type, gen_1(type), fact)), \
          gen_mul(type, val2, fact))

/**
 * @brief Linear interpolation between two values
 *
 * Constant factor version
 *
 * @param[in] type The type of values
 * @param[in] val1 The first value
 * @param[in] val2 The second value
 * @param[in] fact The interpolation factor [0...1]
 * @return The result value
 *
 * If required the both values must be prescaled to common range.
 *
 * res = val1 * (1 - fact) + val2 * fact
 */
#define lerp_val_const(type, val1, val2, fact)       \
  gen_add(type,                                      \
          gen_mul(type, val1,                        \
                  gen_make(type, 1.0 - fact)),       \
          gen_mul(type, val2, gen_make(type, fact)))

/**
 * @brief Reduce value to range
 *
 * @param[in] type The type of values
 * @param[in] val The source value
 * @param[in] min The minimum value
 * @param[in] max The maximum value
 * @return The result value
 *
 * res = val < min ? min : max < val ? max : val
 */
#define clamp_val(type, val, min, max)     \
  (gen_lt(type, val, min) ? (min) :        \
   gen_lt(type, max, val) ? (max) : (val))

/**
 * @brief Reduce value to range
 *
 * Constant range version
 *
 * @param[in] type The type of values
 * @param[in] val The source value
 * @param[in] min The minimum value (float)
 * @param[in] max The maximum value (float)
 * @return The result value
 *
 * res = val < min ? min : max < val ? max : val
 */
#define clamp_val_const(type, val, min, max)    \
  clamp_val(type, val, gen_make(type, min),     \
                       gen_make(type, max))

/**
 * @brief Re-scale value from range [a, b] to range [l, h]
 *
 * @param[in] type The type of values
 * @param[in] val The source value
 * @param[in] a The minimum value of source range
 * @param[in] b The maximum value of source range
 * @param[in] l The minimum value of target range
 * @param[in] h The maximum value of target range
 * @return The result value
 *
 * res = l + (val - a) * (h - l) / (b - a)
 */
#define scale_val(type, val, a, b, l, h)          \
  gen_add(type, l,                                \
          gen_mul(type,                           \
                  gen_sub(type, val, a),          \
                  gen_div(type,                   \
                          gen_sub(type, h, l),    \
                          gen_sub(type, b, a))))

/**
 * @brief Re-scale value from range [a, b] to range [l, h]
 *
 * Constant range version
 *
 * @param[in] type The type of values
 * @param[in] val The source value
 * @param[in] a The minimum value of source range (float)
 * @param[in] b The maximum value of source range (float)
 * @param[in] l The minimum value of target range (float)
 * @param[in] h The maximum value of target range (float)
 * @return The result value
 *
 * res = l + (val - a) * (h - l) / (b - a)
 */
#define scale_val_const(type, val, a, b, l, h)               \
  gen_add(type,                                              \
          gen_mul(type,                                      \
                  gen_sub(type, val,                         \
                          gen_make(type, (double)(a))),      \
                  gen_make(type,                             \
                           ((double)(h) - (double)(l)) /     \
                           ((double)(b) - (double)(a)))),    \
          gen_make(type, (double)(l)))

/**
 * @brief Get the number of elements in arrays
 *
 * @param[in] arr Target array
 * @return The number of elements
 */
#define lenof(arr) (sizeof(arr) / sizeof((arr)[0]))

/**
 * @brief Lookup value in table with linear interpolation
 *
 * @param[in] type The type of values
 * @param[in] table The table to lookup in
 * @param[in] arg The value to lookup
 * @return The result value
 */
#define lookup_val(type, table, arg) ({             \
      type s = gen_mul(type,                        \
                       gen_sub(type, arg,           \
                               table##_from),       \
                       gen_div(type, gen_1(type),   \
                               table##_step));      \
      int i = gen_int(type, s);                     \
                                                    \
      if (i < 0) {                                  \
        i = 0;                                      \
      } else if (i > (int)lenof(table##_data) - 2) {\
        i = lenof(table##_data) - 2;                \
      }                                             \
                                                    \
      gen_add(type, table##_data[i],                \
              gen_mul(type,                         \
                      gen_sub(type, s,              \
                              gen_make(type, i)),   \
                      gen_sub(type,                 \
                              table##_data[i + 1],  \
                              table##_data[i])));   \
    })

/**
 * @brief Reverse lookup value in table with linear interpolation
 *
 * @param[in] type The type of values
 * @param[in] table The table to lookup in
 * @param[in] val The value to lookup
 * @return The argument value
 *
 * Applicable to monotonic functions only.
 */
#define lookup_arg(type, table, val) ({             \
      int i, s, e;                                  \
                                                    \
      if (gen_lt(type, table##_data[0],             \
                 table##_data[lenof(table##_data)   \
                              - 1])) {              \
        i = 1;                                      \
        s = 1;                                      \
        e = lenof(table##_data) - 1;                \
      } else {                                      \
        i = lenof(table##_data) - 2;                \
        s = -1;                                     \
        e = 0;                                      \
      }                                             \
                                                    \
      for (; i != e &&                              \
             gen_gt(type, val, table##_data[i]);    \
           i += s);                                 \
                                                    \
      e = i - s;                                    \
                                                    \
      type part =                                   \
        gen_div(type,                               \
                gen_sub(type, val,                  \
                        table##_data[e]),           \
                gen_sub(type, table##_data[i],      \
                        table##_data[e]));          \
                                                    \
      gen_add(type, table##_from,                   \
              gen_mul(type, table##_step,           \
                      gen_add(type,                 \
                              gen_make(type, e),    \
                              s > 0 ? part :        \
                              gen_neg(type,         \
                                      part))));     \
    })

/**
 * @}
 */

/**
 * @defgroup control Control utils
 * @brief This part implements generic control utilities
 *
 * @{
 */

/**
 * @brief Scale ABC coords from range [f, t] to range [l, h]
 *
 * @param[in] type The type of values
 * @param[in] abc The source value
 * @param[in] f The minimum value of source range
 * @param[in] t The maximum value of source range
 * @param[in] l The minimum value of target range
 * @param[in] h The maximum value of target range
 * @param[out] Sabc The result value
 */
#define scale_abc_const(type, abc, f, t, l, h, Sabc) {         \
    (Sabc)->a = scale_val_const(type, (abc)->a, f, t, l, h);   \
    (Sabc)->b = scale_val_const(type, (abc)->b, f, t, l, h);   \
    (Sabc)->c = scale_val_const(type, (abc)->c, f, t, l, h);   \
  }

/**
 * @brief Calculate magnitude-frequency modulator step
 *
 * @param[in] type The data type to operating
 * @param[in] modulation_period The period of modulation step
 * @param[in] field_frequency The frequency of field rotation
 * @return The step of modulation angle
 *
 * The value is dependent from modulation period and field revolution speed.
 */
#define mf_step(type, modulation_period, field_frequency) \
  gen_mul(type,                                           \
          gen_mul(type,                                   \
                  gen_pi_2(type), modulation_period),     \
          field_frequency)

/**
 * @}
 */

/**
 * @}
 */
#endif /* __CTL__UTIL_H__ */

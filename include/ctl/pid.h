#ifndef __CTL__PID_H__
#define __CTL__PID_H__
/**
 * @ingroup controllers
 * @defgroup pid PID controller
 * @brief This part implements proportional integral derivative (PID) controller
 *
 * @{
 */

#include <ctl/type.h>

/**
 * @brief PID parameters type
 *
 * @param[in] type The type of values
 * @return The PID parameters structure
 */
#define pid_param_t(type)                       \
  struct {                                      \
    /**                                         \
     * @brief Proportional factor (Kp)          \
     */                                         \
    type Kp;                                    \
    /**                                         \
     * @brief Integral factor (1/Ti)            \
     */                                         \
    type Ki;                                    \
    /**                                         \
     * @brief Integral error limit              \
     */                                         \
    type Ei_min, Ei_max;                        \
    /**                                         \
     * @brief Derivative factor (1/Kd)          \
     */                                         \
    type Td;                                    \
  }

/**
 * @brief PID state type
 *
 * @param[in] type The type of values
 * @return The PID state structure
 */
#define pid_state_t(type)                               \
  struct {                                              \
    /**                                                 \
     * @brief Last error                                \
     *                                                  \
     * The difference between target and actual values. \
     */                                                 \
    type E;                                             \
    /**                                                 \
     * @brief Integral of error                         \
     *                                                  \
     * The accumulated error.                           \
     */                                                 \
    type Ei;                                            \
  }

/**
 * @brief Initialize constant PID parameters
 *
 * @param[in] type The type of values
 * @param[in] proportional The proportional factor value
 * @param[in] integral The integral factor time
 * @param[in] derivative The derivative factor value
 * @param[in] integral_min The minimum of integral error
 * @param[in] integral_max The maximum of integral error
 * @param[in] period The sampling time (control step period)
 */
#define pid_make_Kp_Ti_Td_Ei_range(type, proportional, integral, derivative, integral_min, integral_max, period) \
  {                                                                     \
    .Kp = (proportional),                                               \
    .Ki = gen_inv(type, gen_mul(type, (integral), (period))),           \
    .Ei_min = (integral_min),                                           \
    .Ei_max = (integral_max),                                           \
    .Td = gen_mul(type, (derivative), (period)),                        \
  }

/**
 * @brief Initialize constant PID parameters
 *
 * @param[in] type The type of values
 * @param[in] proportional The proportional factor value
 * @param[in] integral The integral factor time
 * @param[in] derivative The derivative factor value
 * @param[in] integral_lim The absolute maximum of integral error
 * @param[in] period The sampling time (control step period)
 */
#define pid_make_Kp_Ti_Td_Ei_limit(type, proportional, integral, derivative, integral_lim, period) \
  pid_make_Kp_Ti_Td_Ei_range(type,                                      \
                             proportional, integral, derivative,        \
                             gen_neg(type, integral_lim),               \
                             integral_lim, period)

/**
 * @brief Set proportional factor value
 *
 * @param[in] type The type of values
 * @param[in,out] param The pointer to PID parameters
 * @param[in] proportional The proportional factor value
 */
#define pid_set_Kp(type, param, proportional)   \
  ((param)->Kp = (proportional))

/**
 * @brief Set integral factor value
 *
 * @param[in] type The type of values
 * @param[in,out] param The pointer to PID parameters
 * @param[in] integral The integral factor value
 *
 * The value of integral factor must be represented in time units.
 * The integral factor value depend from sampling time.
 */
#define pid_set_i(type, param, integral)        \
  ((param)->Ki = gen_inv(type, (integral)))

/**
 * @brief Set integral factor value
 *
 * @param[in] type The type of values
 * @param[in,out] param The pointer to PID parameters
 * @param[in] integral The integral factor time
 * @param[in] period The sampling time (control step period)
 *
 * The value of integral factor must be represented in time units.
 */
#define pid_set_Ti(type, param, integral, period)   \
  pid_set_i(type, param,                            \
            gen_mul(type, integral, period))

/**
 * @brief Limiting integral error to the desired range
 *
 * @param[in] type The type of values
 * @param[in,out] param The pointer to PID parameters
 * @param[in] integral_min The minimum of integral error
 * @param[in] integral_max The maximum of integral error
 */
#define pid_set_Ei_range(type, param, integral_min, integral_max)   \
  ({                                                                \
    (param)->Ei_min = (integral_min);                               \
    (param)->Ei_max = (integral_max);                               \
  })

/**
 * @brief Limiting integral error to the absolute maximum
 *
 * @param[in] type The type of values
 * @param[in,out] param The pointer to PID parameters
 * @param[in] integral_lim The absolute maximum of integral error
 */
#define pid_set_Ei_limit(type, param, integral_lim) \
  pid_set_Ei_range(type, param,                     \
                   gen_neg(type, integral_lim),     \
                   integral_lim)

/**
 * @brief Set derivative factor value
 *
 * @param[in] type The type of values
 * @param[in,out] param The pointer to PID parameters
 * @param[in] derivative The derivative factor value
 *
 * The value of derivative factor must be represented in time units.
 * The derivative factor value depend from sampling time.
 */
#define pid_set_d(type, param, derivative)      \
  ((param)->Td = (derivative))

/**
 * @brief Set derivative factor value
 *
 * @param[in] type The type of values
 * @param[in,out] param The pointer to PID parameters
 * @param[in] derivative The derivative factor value
 * @param[in] period The sampling time (control step period)
 *
 * The value of derivative factor must be represented in time units.
 */
#define pid_set_Td(type, param, derivative, period)   \
  pid_set_d(type, param,                              \
            gen_mul(type, derivative, period))

/**
 * @brief Initialize PID state with zeros
 *
 * @param[in] type The type of values
 * @param[in,out] state The pointer to PID state
 */
#define pid_init(type, state) ({  \
      /* state.E = 0 */           \
      (state)->E = gen_0(type);   \
      /* state.Ei = 0 */          \
      (state)->Ei = gen_0(type);  \
      (state);                    \
    })

/**
 * @brief Evaluate PID control step
 *
 * @param[in] type The type of values
 * @param[in] param The pointer to PID parameters
 * @param[in,out] state The pointer to PID state
 * @param[in] error The actual error value
 * @return The result control value
 */
#define pid_step(type, param, state, error) ({                          \
      /* Ed = error - state.E */                                        \
      type Ed = gen_sub(type, (error), (state)->E);                     \
      /* state.Ei += error */                                           \
      (state)->Ei = clamp_val(type,                                     \
                              gen_add(type, (state)->Ei, (error)),      \
                              (param)->Ei_min,                          \
                              (param)->Ei_max);                         \
      /* state.E = error */                                             \
      (state)->E = (error);                                             \
      /* (error + state.Ei * param.Ki + Ed * param.Td) * param.Kp */    \
      gen_mul(type,                                                     \
              gen_add(type, (error),                                    \
                      gen_add(type,                                     \
                              gen_mul(type, (state)->Ei, (param)->Ki),  \
                              gen_mul(type, Ed, (param)->Td))),         \
              (param)->Kp);                                             \
    })

/**
 * @}
 */
#endif /* __CTL__PID_H__ */

#ifndef __CTL__SVM_H__
#define __CTL__SVM_H__
/**
 * @ingroup converters
 * @defgroup svm Space-vector 3-phase modulation
 * @brief This module implements classic space-vector 3-phase modulation
 *
 * SVM phases:
 *
 * + S0: a,b,c dx
 * + S1: b,a,c dy
 * + S2: b,c,a dx
 * + S3: c,b,a dy
 * + S4: c,a,b dx
 * + S5: a,c,b dy
 *
 * @{
 */

#include <ctl/type.h>

/**
 * @brief The internal state of modulator
 */
#define svm_t(type) struct {                               \
    /**                                                    \
     * @brief The actual magnetic field angle into sector  \
     */                                                    \
    type angle;                                            \
    /**                                                    \
     * @brief The actual sector number                     \
     */                                                    \
    uint8_t sector;                                        \
  }

/**
 * @brief Initialize modulator state
 *
 * @param[in] type The scalar type to operate
 * @param[out] state The pointer to modulator state
 */
#define svm_init(type, state) {    \
    (state)->angle = gen_0(type);  \
    (state)->sector = 0;           \
  }

/**
 * @brief Run calculation of duty cycle values
 *
 * You must call this function synchronous with control frequency.
 *
 * @param[in] type The scalar type to operate
 * @param[in] mf_step The magnitude-frequency step
 * @param[in,out] state The pointer to modulator state
 * @param[out] abc The pointer to modulator output
 */
#define svm_step(type, mf_step, state, abc) {       \
    /* angle += step */                             \
    (state)->angle = gen_add(type, (state)->angle,  \
                             (mf_step));            \
                                                    \
    /* angle >= PI / 3 */                           \
    if (!gen_lt(type, (state)->angle,               \
                gen_pi_inv_3(type))) {              \
      /* angle -= PI / 3 */                         \
      (state)->angle = gen_sub(type,                \
                               (state)->angle,      \
                               gen_pi_inv_3(type)); \
      if ((state)->sector < 5) {                    \
        (state)->sector ++;                         \
      } else {                                      \
        (state)->sector = 0;                        \
      }                                             \
    }                                               \
                                                    \
    /* dx = sin(PI / 3 - angle) */                  \
    type dx = gen_sin(type,                         \
                      gen_sub(type,                 \
                              gen_pi_inv_3(type),   \
                              (state)->angle));     \
    /* dy = sin(angle) */                           \
    type dy = gen_sin(type, (state)->angle);        \
                                                    \
    uint8_t p0; uint8_t p1; uint8_t p2;             \
    switch ((state)->sector) {                      \
    case 0: p0 = 0; p1 = 1; p2 = 2; break;          \
    case 1: p0 = 1; p1 = 0; p2 = 2; break;          \
    case 2: p0 = 1; p1 = 2; p2 = 0; break;          \
    case 3: p0 = 2; p1 = 1; p2 = 0; break;          \
    case 4: p0 = 2; p1 = 0; p2 = 1; break;          \
    case 5: p0 = 0; p1 = 2; p2 = 1; break;          \
    }                                               \
                                                    \
    /* p2 = dx + dy */                              \
    (abc)->_[p2] = gen_add(type, dx, dy);           \
    /* p0 = -p2 */                                  \
    (abc)->_[p0] = gen_neg(type, (abc)->_[p2]);     \
    /* p1 = p0 + 2 * (sector % 2 ? dy : dx) */      \
    (abc)->_[p1] =                                  \
      gen_add(type, (abc)->_[p0],                   \
              gen_mul(type, gen_2(type),            \
                      (state)->sector & 1 ?         \
                      dy : dx));                    \
  }

#define svm_p0(sector) (((sector+1)>>1)%3)
#define svm_p1(sector) (2-((sector+1)%3))
#define svm_p2(sector) (((sector+4)>>1)%3)

/**
 * @}
 */
#endif /* __CTL__SVM_H__ */

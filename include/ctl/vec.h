#ifndef __CTL__VEC_H__
#define __CTL__VEC_H__
/**
 * @ingroup operation
 * @defgroup vec Vector operations
 * @brief This module implements generic numeric vector operations
 *
 * @{
 */

/**
 * @brief Get the number of elements in vector
 *
 * @param[in] vec The target vector
 * @return The number of elements
 */
#define vec_size(vec)              \
  (sizeof(vec) / sizeof((vec)[0]))

/**
 * @brief Pick element of vector by index
 *
 * Generic vector accessor for arrays
 *
 * @param[in] vec The target vector
 * @param[in] idx The index of the element
 * @param[in] ... The arguments for accessor
 * @return The picked value
 */
#define vec_pick(vec, idx, ...) (((vec)[(idx)]) __VA_ARGS__)

/**
 * @brief Swap two elements of vector
 *
 * @param[in,out] vec The target vector
 * @param[in] i The index of first element to swap
 * @param[in] j The index of second element to swap
 * @param[in] ... The arguments for accessor
 */
#define vec_swap(vec, i, j, ...) ({                  \
      typeof(vec_pick(vec, 0, ##__VA_ARGS__)) __v =  \
        vec_pick(vec, i, ##__VA_ARGS__);             \
      vec_pick(vec, i, ##__VA_ARGS__) =              \
        vec_pick(vec, j, ##__VA_ARGS__);             \
      vec_pick(vec, j, ##__VA_ARGS__) = __v;         \
      (vec);                                         \
    })

/**
 * @brief Sort values in ascending order
 */
#define vec_sort_asc gen_gt

/**
 * @brief Sort values in descending order
 */
#define vec_sort_desc gen_lt

/**
 * @brief Sort values in vector using comb sort
 *
 * @param[in] type The type of values
 * @param[in] cmp The comparison operator
 * @param[in,out] vec The vector to sort
 * @param[in] len The number of values to sort
 * @param[in] ... The arguments for accessor
 */
#define vec_sort(type, cmp, vec, len, ...) {        \
    uint16_t last = (len) - 1;                      \
    uint16_t step = last;                           \
    uint16_t i, j;                                  \
                                                    \
    for (; step >= 1; ) {                           \
      uint16_t top = (len) - step;                  \
                                                    \
      for (i = 0; i < top; ++ i) {                  \
        if (cmp(type,                               \
                vec_pick(vec, i, ##__VA_ARGS__),    \
                vec_pick(vec, i + step,             \
                            ##__VA_ARGS__))) {      \
          vec_swap(vec, i, i + step,                \
                            ##__VA_ARGS__);         \
        }                                           \
      }                                             \
                                                    \
      step = gen_int(fix8,                          \
                     gen_mul(fix8,                  \
                             gen_make(fix8, step),  \
                             gen_make(fix8, 0.8))); \
    }                                               \
                                                    \
    /* bubble sort */                               \
    for (i = 0; i < last; i ++) {                   \
      char swapped = 0;                             \
                                                    \
      for (j = 0; j < last - i; j ++) {             \
        if (cmp(type,                               \
                vec_pick(vec, j, ##__VA_ARGS__),    \
                vec_pick(vec, j + 1,                \
                         ##__VA_ARGS__))) {         \
          vec_swap(vec, j, j + 1, ##__VA_ARGS__);   \
          swapped = 1;                              \
        }                                           \
      }                                             \
                                                    \
      if (!swapped) {                               \
        break;                                      \
      }                                             \
    }                                               \
  }

/**
 * @}
 */
#endif /* __CTL__VEC_H__ */

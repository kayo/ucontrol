#ifndef __CTL__DL_H__
#define __CTL__DL_H__
/**
 * @ingroup filters
 * @defgroup delay Delay line
 * @brief The delay line for filters
 *
 * @{
 */

#include <ctl/type.h>

/**
 * @brief Delay line state type
 *
 * @param[in] type The value type to store
 * @param[in] size The length of line
 */
#define dl_t(type, size)            \
  struct {                          \
    /**                             \
     * @brief The stored values     \
     */                             \
    type buf[size];                 \
    /**                             \
     * @brief The last position     \
     */                             \
    uint8_t pos;                    \
    /**                             \
     * @brief The number of values  \
     */                             \
    uint8_t num;                    \
  }

/**
 * @brief Get length of delay line
 *
 * @param[in] dl The pointer to line
 * @return Maximum number of values on line
 */
#define dl_size(dl)                 \
  (sizeof((dl)->buf) /              \
   sizeof((dl)->buf[0]))

/**
 * @brief Initialize delay line
 *
 * @param[in] dl The pointer to line
 */
#define dl_init(dl) {               \
    (dl)->pos = dl_size(dl) - 1;    \
    (dl)->num = 0;                  \
  }

/**
 * @brief Push value to delay-line
 *
 * @param[in] dl The pointer to line
 * @param[in] val The value to push
 */
#define dl_push(dl, val) {              \
    if ((dl)->pos < dl_size(dl) - 1) {  \
      (dl)->pos ++;                     \
    } else {                            \
      (dl)->pos = 0;                    \
    }                                   \
                                        \
    (dl)->buf[(dl)->pos] = (val);       \
                                        \
    if ((dl)->num < dl_size(dl)) {      \
      (dl)->num ++;                     \
    }                                   \
  }

/**
 * @brief Check the readiness of delay line
 *
 * @param[in] dl The pointer to line
 * @return The status of line
 *
 * When the number of values on delay line equals to line length
 * the line is ready to use.
 */
#define dl_ready(dl)                \
  ((dl)->num == dl_size(dl))

/**
 * @brief Check the readiness of delay line
 *
 * @param[in] dl The pointer to line
 * @param[in] req The required number of values on line
 * @return The status of line
 *
 * When the number of values on delay line equals to required number
 * the line is ready to use.
 */
#define dl_depth(dl, req)           \
  ((dl)->num >= (req))

/**
 * @brief Read values from delay line
 *
 * @param[in] dl The pointer to line
 * @param[in] idx The index (depth) of value
 *
 * The index must not to be more than length of line.
 * You shouldn't read values until line is ready to use.
 */
#define dl_read(dl, idx)                       \
  ((dl)->buf[((dl)->pos < (idx)) ?             \
             ((dl)->num + (dl)->pos - (idx)) : \
             ((dl)->pos - (idx))])

/**
 * @}
 */
#endif /* __CTL__DL_H__ */

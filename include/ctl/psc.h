#ifndef __CTL__PSC_H__
#define __CTL__PSC_H__
/**
 * @ingroup converters
 * @defgroup psc 3-phase phase-shift correction
 * @brief This part implements phase-shift correction (compensation).
 * 
 * Phase-shift PWM correction can help to improve final wave-form for phases current measurement using single current sensor.
 *
 * @{
 */

#include <ctl/type.h>
#include <ctl/debug.h>

/**
 * @brief The corrector parameters type
 *
 * @param[in] type The type to operate
 */
#define psc_t(type)                                       \
  struct {                                                \
    /**                                                   \
     * @brief The minimal relative time for compensation  \
     */                                                   \
    type delay;                                           \
  }

/**
 * @brief Initialize corrector state
 *
 * @param[in] type The type to operate
 * @param[out] state The pointer to state (abc_t)
 */
#define psc_init(type, state) { \
    (state)->a = gen_0(type);   \
    (state)->b = gen_0(type);   \
    (state)->c = gen_0(type);   \
  }

/**
 * @brief Configure corrector delay
 *
 * @param[in] type The type to operate
 * @param[out] param The pointer to parameters
 * @param[in] period The modulation period
 * @param[in] time The critical time to adjust to
 */
#define psc_set(type, param, period, time) {     \
    (param)->delay = gen_div(type,               \
                             time,               \
                             period);            \
  }

/**
 * @brief Run correction of duty cycle values
 *
 * @param[in] type The type to operate
 * @param[in] param The pointer to parameters
 * @param[in,out] state The pointer to corrector state
 * @param[in,out] abc The pointer to pwm values
 * @param[out] iab The pointer to 2-phase indexes
 *
 * The pointer to phase indexes can be NULL.
 * In which case the correction won't be performed,
 * but the effect of previous correction will be applied in any case.
 */
#define psc_step(type, param, state, abc, iab) {        \
    uint8_t k = 0;                                      \
                                                        \
    for (; k < 3; k++) {                                \
      type *Tp = &(abc)->_[k];                          \
      type *dTp = &(state)->_[k];                       \
                                                        \
      if (gen_gt(type, *dTp, gen_0(type)) ||            \
          gen_lt(type, *dTp, gen_0(type))) {            \
        ctl_debug("postadj phase=%u delta=%f\n",        \
                  k, gen_float(type, *dTp));            \
      }                                                 \
                                                        \
      *Tp = gen_add(type, *Tp, *dTp);                   \
      if (gen_lt(type, *Tp, gen_0(type))) {             \
        *dTp = *Tp;                                     \
        *Tp = gen_0(type);                              \
      } else if (gen_gt(type, *Tp, gen_1(type))) {      \
        *dTp = gen_sub(type, *Tp, gen_1(type));         \
        *Tp = gen_1(type);                              \
      } else {                                          \
        *dTp = gen_0(type);                             \
      }                                                 \
    }                                                   \
                                                        \
    if ((iab) != NULL) {                                \
      /*                                                \
        We may measure two phase currents:              \
        +I[max]                                         \
        -I[min]                                         \
                                                        \
        The time intervals for measurements:            \
        abc->_[max] - abc->_[mid]                       \
        abc->_[mid] - abc->_[min]                       \
      */                                                \
      uint8_t mid;                                      \
      /* iab->a as max, iab->b as min */                \
                                                        \
      if (gen_lt(type,                                  \
                 (abc)->a,                              \
                 (abc)->b)) { /* Ta < Tb */             \
        if (gen_lt(type,                                \
                   (abc)->a,                            \
                   (abc)->c)) { /* Ta < Tb,Tc */        \
          (iab)->b = abc_a;                             \
          if (gen_lt(type,                              \
                     (abc)->b,                          \
                     (abc)->c)) { /* Ta < Tb < Tc */    \
            mid = abc_b;                                \
            (iab)->a = abc_c;                           \
          } else { /* Ta < Tc < Tb */                   \
            mid = abc_c;                                \
            (iab)->a = abc_b;                           \
          }                                             \
        } else { /* Tc < Ta < Tb */                     \
          (iab)->b = abc_c;                             \
          mid = abc_a;                                  \
          (iab)->a = abc_b;                             \
        }                                               \
      } else { /* Tb < Ta */                            \
        if (gen_lt(type,                                \
                   (abc)->b,                            \
                   (abc)->c)) { /* Tb < Ta,Tc */        \
          (iab)->b = abc_b;                             \
          if (gen_lt(type,                              \
                     (abc)->a,                          \
                     (abc)->c)) { /* Tb < Ta < Tc */    \
            mid = abc_a;                                \
            (iab)->a = abc_c;                           \
          } else { /* Tb < Tc < Ta */                   \
            mid = abc_c;                                \
            (iab)->a = abc_a;                           \
          }                                             \
        } else { /* Tc < Tb < Ta */                     \
          (iab)->b = abc_c;                             \
          mid = abc_b;                                  \
          (iab)->a = abc_a;                             \
        }                                               \
      }                                                 \
                                                        \
      /*                                                \
       :   ________:________   :                        \
    A  :__|        :        |__:                        \
   max :  .   _____:_____      :                        \
    B  :_____|     :     |_____:                        \
   mid :  .  .   __:__         :                        \
    C  :________|  :  |________:                        \
   min :  .  .  .  :           :                        \
   T1 T2                                                \
      */                                                \
                                                        \
      type T1 = gen_sub(type,                           \
                        (abc)->_[(iab)->a],             \
                        (abc)->_[mid]);                 \
      type T2 = gen_sub(type,                           \
                        (abc)->_[mid],                  \
                        (abc)->_[(iab)->b]);            \
                                                        \
      ctl_debug("Tcrit=%f T1=%f T2=%f\n",               \
                gen_float(type, (param)->delay),        \
                gen_float(type, T1),                    \
                gen_float(type, T2));                   \
                                                        \
      type dT = gen_0(type);                            \
                                                        \
      if (gen_lt(type, T1, (param)->delay)) {           \
        dT = gen_sub(type, (param)->delay, T1);         \
      } else if (gen_lt(type, T2, (param)->delay)) {    \
        dT = gen_sub(type, T2, (param)->delay);         \
      }                                                 \
                                                        \
      if (dT != gen_0(type)) {                          \
        type *Tp = &(abc)->_[mid];                      \
        type *dTp = &(state)->_[mid];                   \
                                                        \
        *Tp = gen_sub(type, *Tp, dT);                   \
        *dTp = gen_add(type, *dTp, dT);                 \
      }                                                 \
    }                                                   \
  }

/*
  The inverter scheme:

  DC+ ----o-------o-------o
          |       |       |
          I        \       \
          |       |       |
       A (+)   B (-)   C (-)
          |       |       |
           \      I       I
          |       |       |
  DC- ----o-------o-------o

  The current table:

  | Sa | Sb | Sc | Idc |
  |----|----|----|-----|
  | +  | -  | -  | +Ia |
  | -  | +  | +  | -Ia |
  |    |    |    |     |
  | -  | +  | -  | +Ib |
  | +  | -  | +  | -Ib |
  |    |    |    |     |
  | -  | -  | +  | +Ic |
  | +  | +  | -  | -Ic |
  |    |    |    |     |
  | -  | -  | -  | 0   |
  | +  | +  | +  | 0   |
  
 */

/**
 * @}
 */
#endif /* __CTL__PSC_H__ */

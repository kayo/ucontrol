#ifndef __CTL__LQE_H__
#define __CTL__LQE_H__
/**
 * @ingroup filters
 * @defgroup lqe LQE (Kalman) filter
 * @brief This part implements linear quadratic estimation (LQE) filtering which also known as Kalman filter.
 *
 * @{
 */

#include <ctl/type.h>

/**
 * @brief Filter parameters type
 *
 * @param[in] type The type of values
 * @return The filter parameters structure
 */
#define lqe_param_t(type)                                             \
  struct {                                                            \
    /**                                                               \
     * @brief The factor of actual value to previous actual value     \
     */                                                               \
    type F;                                                           \
    /**                                                               \
     * @brief The factor of measured value to actual value            \
     */                                                               \
    type H;                                                           \
    /**                                                               \
     * @brief The measurement noise                                   \
     */                                                               \
    type Q;                                                           \
    /**                                                               \
     * @brief The environment noise                                   \
     */                                                               \
    type R;                                                           \
  }

/**
 * @brief Set factor of actual value to previous actual value
 *
 * @param[in] type The type of values
 * @param[in,out] param The pointer to filter parameters
 * @param[in] f The factor value
 */
#define lqe_set_F(type, param, f)               \
  ((param)->F = (f))

/**
 * @brief Set factor of actual value to previous actual value
 *
 * @param[in] type The type of values
 * @param[in,out] param The pointer to filter parameters
 * @param[in] f The factor value
 * @param[in] period The sampling time (control step period)
 */
#define lqe_set_F_period(type, param, f, period)        \
  lqe_set_F(type, param, gen_mul(type, (f), (period)))

/**
 * @brief Set factor of measured value to actual value
 *
 * @param[in] type The type of values
 * @param[in,out] param The pointer to filter parameters
 * @param[in] h The factor value
 */
#define lqe_set_H(type, param, h)            \
  ((param)->H = (h))

/**
 * @brief Set measurement noise
 *
 * @param[in] type The type of values
 * @param[in,out] param The pointer to filter parameters
 * @param[in] q The factor value
 */
#define lqe_set_Q(type, param, q)           \
  ((param)->Q = (q))

/**
 * @brief Set environment noise
 *
 * @param[in] type The type of values
 * @param[in,out] param The pointer to filter parameters
 * @param[in] r The factor value
 */
#define lqe_set_R(type, param, r)            \
  ((param)->R = (r))

/**
 * @brief Filter state type
 *
 * @param[in] type The type of values
 * @return The filter parameters structure
 */
#define lqe_state_t(type) \
  struct {                   \
    /**                      \
     * @brief The state      \
     */                      \
    type X;                  \
    /**                      \
     * @brief The covariance \
     */                      \
    type P;                  \
  }

/**
 * @brief Initialize filter state
 *
 * @param[in] type The type of values
 * @param[in,out] state The pointer to filter state
 * @param[in] x The initial source value
 * @param[in] p The initial covariance
 */
#define lqe_init(type, state, x, p)    \
  ({                                   \
    (state)->P = (p);                  \
    (state)->X = (x);                  \
  })

/**
 * @brief Evaluate filtering step
 *
 * @param[in] type The type of values
 * @param[in] param The pointer to filter parameters
 * @param[in,out] state The pointer to filter state
 * @param[in] x The source value
 * @return The filtered value
 */
#define lqe_step(type, param, state, x)                 \
  ({                                                    \
    /* prediction: */                                   \
    /* predicted state */                               \
    /* X0 = param.F * state.X */                        \
    type X0 = gen_mul(type, (param)->F, (state)->X);    \
    /* predicted covariance */                          \
    /* P0 = param.F^2 * state.P + param.Q */            \
    type P0 = gen_add(type,                             \
                      gen_mul(type,                     \
                              gen_mul(type,             \
                                      (param)->F,       \
                                      (param)->F),      \
                              (state)->P),              \
                      (param)->Q);                      \
    /* correction: */                                   \
    /* K = param.H * P0 / (param.H^2 * P0 + param.R) */ \
    type K =                                            \
      gen_div(type,                                     \
              gen_mul(type, (param)->H, P0),            \
              gen_add(type,                             \
                      gen_mul(type,                     \
                              gen_mul(type,             \
                                      (param)->H,       \
                                      (param)->H),      \
                              P0),                      \
                      (param)->R));                     \
    /* state.P = (1 - K * param.H) * P0 */              \
    (state)->P =                                        \
      gen_mul(type,                                     \
              gen_sub(type,                             \
                      gen_1(type),                      \
                      gen_mul(type, K, (param)->H)),    \
              P0);                                      \
    /* state.X = X0 + K * (X - param.H * X0) */         \
    (state)->X =                                        \
      gen_add(type, X0,                                 \
              gen_mul(type, K,                          \
                      gen_sub(type, x,                  \
                              gen_sub(type,             \
                                      (param)->H,       \
                                      X0))));           \
  })

/**
 * @}
 */
#endif /* __CTL__LQE_H__ */

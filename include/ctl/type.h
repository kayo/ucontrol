#ifndef __CTL__TYPE_H__
#define __CTL__TYPE_H__
/**
 * @addtogroup numbers
 * @brief This part implements generic numeric types and math operations.
 */

#include <stdint.h>
#include <math.h>

/**
 * @ingroup internals
 * @defgroup fixT Types
 * @brief The generic numeric types
 * @{
 */

/**
 * @brief Generic fixed point type
 */
typedef int32_t fixed;
typedef int64_t dfixed;

/**
 * @}
 */

/**
 * @ingroup numbers
 * @defgroup types Types
 * @brief The generic numeric types
 * @{
 */

/**
 * @defgroup scalar Scalar
 * @brief The scalar numeric types
 * @{
 */

/**
 * @brief The Q24.8 fixed point type
 */
typedef fixed fix8;

/**
 * @brief The Q16.16 fixed point type
 */
typedef fixed fix16;

/**
 * @brief The Q8.24 fixed point type
 */
typedef fixed fix24;

/**
 * @}
 */

/**
 * @}
 */

/**
 * @ingroup numbers
 * @defgroup operation Operations
 * @brief The generic numeric operations
 * @{
 */

/**
 * @brief Apply generic operation
 *
 * @param[in] type The type of values
 * @param[in] op The name of operation
 * @param[in] ... The operands
 * @return The result of operation
 *
 * Currently supported types:
 *
 * @li float - the platform native floating point number
 * @li fix8 - the 32-bit fixed point number with 8 bits in fraction part
 * @li fix16 - the 32-bit fixed point number with 16 bits in fraction part
 * @li fix24 - the 32-bit fixed point number with 24 bits in fraction part
 *
 * Currently supported operations:
 *
 * @li float - convert value to float
 * @li int - convert value to int
 * @li make - make value from float or int
 * @li add - add two values
 * @li sub - substract two values
 * @li mul - multiply two values
 * @li div - divide two values
 * @li lt, gt, le, ge - compare two values
 * @li sin - trigonometric sine
 * @li cos - trigonometric cosine
 */
#define gen_op(type, op, ...) type##_##op(__VA_ARGS__)

/**
 * @defgroup cast Conversions
 * @brief The conversion between numeric values of different types
 * @{
 */

/**
 * @brief Convert generic number to float
 *
 * @param[in] type The type of number
 * @param[in] a The generic number
 * @return The floating point number
 */
#define gen_float(type, a) gen_op(type, float, a)

/**
 * @brief Convert generic number to integer
 *
 * @param[in] type The type of number
 * @param[in] a The generic number
 * @return The resulting integer value
 */
#define gen_int(type, a) gen_op(type, int, a)

/**
 * @brief Create generic numeric value
 *
 * @param[in] type The type of number
 * @param[in] a The floating point or integer number
 * @return The generic numeric value
 */
#define gen_make(type, a) gen_op(type, make, a)

/**
 * @brief Convert generic numbers
 *
 * @param[in] type The source type of number to convert from
 * @param[in] rtype The target type of number to convert to
 * @param[in] a The source value
 * @return The target value
 */
#define gen_to(type, rtype, a) gen_op(type, rtype, a)

/**
 * @}
 */

/**
 * @defgroup arithmetic
 * @brief The arithmetic operations with numeric values
 * @{
 */

/**
 * @brief Sum of two generic numbers (`+` operator)
 *
 * @param[in] type The numeric type to operate
 * @param[in] a The first argument
 * @param[in] b The second argument
 * @return The resulting value
 */
#define gen_add(type, a, b) gen_op(type, add, a, b)

/**
 * @brief Difference of two generic numbers (`-` operator)
 *
 * @param[in] type The numeric type to operate
 * @param[in] a The first argument
 * @param[in] b The second argument
 * @return The resulting value
 */
#define gen_sub(type, a, b) gen_op(type, sub, a, b)

/**
 * @brief Multiplication of two generic numbers (`*` operator)
 *
 * @param[in] type The numeric type to operate
 * @param[in] a The first argument
 * @param[in] b The second argument
 * @return The resulting value
 */
#define gen_mul(type, a, b) gen_op(type, mul, a, b)

/**
 * @brief Division of two generic numbers (`/` operator)
 *
 * @param[in] type The numeric type to operate
 * @param[in] a The first argument
 * @param[in] b The second argument
 * @return The resulting value
 */
#define gen_div(type, a, b) gen_op(type, div, a, b)

/**
 * @brief The negation of generic numeric value
 *
 * @param[in] type The numeric type to operate
 * @param[in] a The source value
 * @return The resulting value (-a)
 */
#define gen_neg(type, a) gen_sub(type, gen_0(type), a)

/**
 * @brief The inversion of generic numeric value
 *
 * @param[in] type The numeric type to operate
 * @param[in] a The source value
 * @return The resulting value (1/a)
 */
#define gen_inv(type, a) gen_div(type, gen_1(type), a)

/**
 * @}
 */

/**
 * @defgroup comparison
 * @brief The comparison of numeric values
 * @{
 */

/**
 * @brief Comparison of two generic numbers (`<` operator)
 *
 * @param[in] type The numeric type to operate
 * @param[in] a The first argument
 * @param[in] b The second argument
 * @return The result of comparison
 */
#define gen_lt(type, a, b) gen_op(type, lt, a, b)

/**
 * @brief Comparison of two generic numbers (`>` operator)
 *
 * @param[in] type The numeric type to operate
 * @param[in] a The first argument
 * @param[in] b The second argument
 * @return The result of comparison
 */
#define gen_gt(type, a, b) gen_op(type, lt, b, a)

/**
 * @brief Comparison of two generic numbers (`>=` operator)
 *
 * @param[in] type The numeric type to operate
 * @param[in] a The first argument
 * @param[in] b The second argument
 * @return The result of comparison
 */
#define gen_ge(type, a, b) (!gen_lt(type, a, b))

/**
 * @brief Comparison of two generic numbers (`<=` operator)
 *
 * @param[in] type The numeric type to operate
 * @param[in] a The first argument
 * @param[in] b The second argument
 * @return The result of comparison
 */
#define gen_le(type, a, b) (!gen_gt(type, a, b))

/**
 * @}
 */

/**
 * @defgroup trigonometry
 * @brief The trigonometric functions
 * @{
 */

/**
 * @brief The sinus of generic numeric value
 *
 * @param[in] type The numeric type to operate
 * @param[in] a The source value
 * @return The resulting value
 */
#define gen_sin(type, a) gen_op(type, sin, a)

/**
 * @brief The cosinus of generic numeric value
 *
 * @param[in] type The numeric type to operate
 * @param[in] a The source value
 * @return The resulting value
 */
#define gen_cos(type, a) gen_op(type, cos, a)

/**
 * @}
 */

/**
 * @}
 */

/**
 * @ingroup numbers
 * @defgroup const Constants
 * @brief Generic numeric constants
 * @{
 */

/**
 * @brief The generic numeric constant (0)
 *
 * @param[in] type The numeric type
 * @return The constant value
 */
#define gen_0(type) gen_make(type, 0.0)

/**
 * @brief The generic numeric constant (1)
 *
 * @param[in] type The numeric type
 * @return The constant value
 */
#define gen_1(type) gen_make(type, 1.0)

/**
 * @brief The generic numeric constant (2)
 *
 * @param[in] type The numeric type
 * @return The constant value
 */
#define gen_2(type) gen_make(type, 2.0)

/**
 * @brief The generic numeric constant (3)
 *
 * @param[in] type The numeric type
 * @return The constant value
 */
#define gen_3(type) gen_make(type, 3.0)

/**
 * @brief The generic numeric constant (π)
 *
 * @param[in] type The numeric type
 * @return The constant value
 */
#define gen_pi(type) gen_make(type, M_PI)

/**
 * @brief The generic numeric constant (e)
 *
 * @param[in] type The numeric type
 * @return The constant value
 */
#define gen_e(type) gen_make(type, M_E)

/**
 * @brief The generic numeric constant (2π)
 *
 * @param[in] type The numeric type
 * @return The constant value
 */
#define gen_pi_2(type) gen_make(type, 2.0 * M_PI)

/**
 * @brief The generic numeric constant (1/2)
 *
 * @param[in] type The numeric type
 * @return The constant value
 */
#define gen_inv_2(type) gen_make(type, 1.0 / 2.0)

/**
 * @brief The generic numeric constant (1/3)
 *
 * @param[in] type The numeric type
 * @return The constant value
 */
#define gen_inv_3(type) gen_make(type, 1.0 / 3.0)

/**
 * @brief The generic numeric constant (1/π)
 *
 * @param[in] type The numeric type
 * @return The constant value
 */
#define gen_inv_pi(type) gen_make(type, 1.0 / M_PI)

/**
 * @brief The generic numeric constant (π/2)
 *
 * @param[in] type The numeric type
 * @return The constant value
 */
#define gen_pi_inv_2(type) gen_make(type, M_PI / 2.0)

/**
 * @brief The generic numeric constant (π/3)
 *
 * @param[in] type The numeric type
 * @return The constant value
 */
#define gen_pi_inv_3(type) gen_make(type, M_PI / 3.0)

/**
 * @brief The generic numeric constant (π/4)
 *
 * @param[in] type The numeric type
 * @return The constant value
 */
#define gen_pi_inv_4(type) gen_make(type, M_PI / 4.0)

/**
 * @brief The generic numeric constant (π/6)
 *
 * @param[in] type The numeric type
 * @return The constant value
 */
#define gen_pi_inv_6(type) gen_make(type, M_PI / 6.0)

/**
 * @brief The generic numeric constant (2π/3)
 *
 * @param[in] type The numeric type
 * @return The constant value
 */
#define gen_pi_2_inv_3(type) gen_make(type, 2.0 * M_PI / 3.0)

/**
 * @brief The generic numeric constant (√2)
 *
 * @param[in] type The numeric type
 * @return The constant value
 */
#define gen_sqrt_2(type) gen_make(type, sqrt(2.0))

/**
 * @brief The generic numeric constant (√3)
 *
 * @param[in] type The numeric type
 * @return The constant value
 */
#define gen_sqrt_3(type) gen_make(type, sqrt(3.0))

/**
 * @brief The generic numeric constant (√2/2)
 *
 * @param[in] type The numeric type
 * @return The constant value
 */
#define gen_sqrt_2_inv_2(type) gen_make(type, sqrt(2.0) / 2.0)

/**
 * @brief The generic numeric constant (√3/2)
 *
 * @param[in] type The numeric type
 * @return The constant value
 */
#define gen_sqrt_3_inv_2(type) gen_make(type, sqrt(3.0) / 2.0)

/**
 * @brief The generic numeric constant (1/√2)
 *
 * @param[in] type The numeric type
 * @return The constant value
 */
#define gen_inv_sqrt_2(type) gen_make(type, 1.0 / sqrt(2.0))

/**
 * @brief The generic numeric constant (1/√3)
 *
 * @param[in] type The numeric type
 * @return The constant value
 */
#define gen_inv_sqrt_3(type) gen_make(type, 1.0 / sqrt(3.0))

/**
 * @}
 */

/**
 * @ingroup internals
 * @defgroup float The floating point operations
 * @{
 */
#define float_float(a) (a)
#define float_int(a) ((int32_t)(a))
#define float_make(a) (a)
#define float_fix8(a) fix8_make(a)
#define float_fix16(a) fix16_make(a)
#define float_fix24(a) fix24_make(a)
#define float_add(a, b) ((a) + (b))
#define float_sub(a, b) ((a) - (b))
#define float_mul(a, b) ((a) * (b))
#define float_div(a, b) ((a) / (b))
#define float_lt(a, b) ((a) < (b))
#define float_sin(a) sin(a)
#define float_cos(a) cos(a)
/**
 * @}
 */

/**
 * @ingroup internals
 * @defgroup fixed The fixed point operations
 * @{
 */

/**
 * @defgroup fixQ The generic fixed point operations
 * @{
 */
#define fixed_float(a, q) ((a) / (float)(1 << (q)))
#define fixed_int(a, q) ((a) >> (q))
#define fixed_make(a, q) ((fixed)((a) * (1 << (q))))
#define fixed_to(a, q, rq) ((rq) > (q) ? ((a) << ((rq) - (q))) : ((a) >> ((q) - (rq))))
#define fixed_add(a, b) ((a) + (b))
#define fixed_sub(a, b) ((a) - (b))
#define fixed_mul(a, b, q) ((fixed)(((dfixed)(a) * (dfixed)(b)) >> (q)))
#define fixed_div(a, b, q) ((fixed)(((dfixed)(a) << (q)) / (dfixed)(b)))
#define fixed_lt(a, b) ((a) < (b))
/**
 * @}
 */

/**
 * @defgroup fix8 The Q24.8 fixed point operations
 * @{
 */

#define fix8_float(a) fixed_float(a, 8)
#define fix8_int(a) fixed_int(a, 8)
#define fix8_make(a) fixed_make(a, 8)
#define fix8_fix8(a) (a)
#define fix8_fix16(a) fixed_to(a, 8, 16)
#define fix8_fix24(a) fixed_to(a, 8, 24)
#define fix8_add(a, b) fixed_add(a, b)
#define fix8_sub(a, b) fixed_sub(a, b)
#define fix8_mul(a, b) fixed_mul(a, b, 8)
#define fix8_div(a, b) fixed_div(a, b, 8)
#define fix8_lt(a, b) fixed_lt(a, b)
fix8 fix8_sin(fix8 a);
fix8 fix8_cos(fix8 a);
/**
 * @}
 */

/**
 * @defgroup fix16 The Q16.16 fixed point operations
 * @{
 */

#define fix16_float(a) fixed_float(a, 16)
#define fix16_int(a) fixed_int(a, 16)
#define fix16_make(a) fixed_make(a, 16)
#define fix16_fix8(a) fixed_to(a, 16, 8)
#define fix16_fix16(a) (a)
#define fix16_fix24(a) fixed_to(a, 16, 24)
#define fix16_add(a, b) fixed_add(a, b)
#define fix16_sub(a, b) fixed_sub(a, b)
#define fix16_mul(a, b) fixed_mul(a, b, 16)
#define fix16_div(a, b) fixed_div(a, b, 16)
#define fix16_lt(a, b) fixed_lt(a, b)
fix16 fix16_sin(fix16 a);
fix16 fix16_cos(fix16 a);
/**
 * @}
 */

/**
 * @defgroup fix24 The Q8.24 fixed point operations
 * @{
 */

#define fix24_float(a) fixed_float(a, 24)
#define fix24_int(a) fixed_int(a, 24)
#define fix24_make(a) fixed_make(a, 24)
#define fix24_fix8(a) fixed_to(a, 24, 8)
#define fix24_fix16(a) fixed_to(a, 24, 16)
#define fix24_fix24(a) (a)
#define fix24_add(a, b) fixed_add(a, b)
#define fix24_sub(a, b) fixed_sub(a, b)
#define fix24_mul(a, b) fixed_mul(a, b, 24)
#define fix24_div(a, b) fixed_div(a, b, 24)
#define fix24_lt(a, b) fixed_lt(a, b)
fix24 fix24_sin(fix24 a);
fix24 fix24_cos(fix24 a);
/**
 * @}
 */

/**
 * @}
 */

/**
 * @ingroup types
 * @defgroup vector Vector
 * @brief The coordinate types
 * @{
 */

/**
 * @brief The @p αβ indexes
 */
enum {
  /**
   * @brief The @p α coordinate index
   */
  ab_a = 0,
  /**
   * @brief The @p β coordinate index
   */
  ab_b = 1,
};

/**
 * @brief The @p αβ vector data
 */
#define ab_t(type) union {     \
    struct {                   \
      /**                      \
       * @brief The @p α value \
       */                      \
      type a;                  \
      /**                      \
       * @brief The @p β value \
       */                      \
      type b;                  \
    };                         \
    /**                        \
     * @brief The values array \
     */                        \
    type _[2];                 \
  }

/**
 * @brief The @p ABC indexes
 */
enum {
  /**
   * @brief The @p A coordinate index
   */
  abc_a = 0,
  /**
   * @brief The @p B coordinate index
   */
  abc_b = 1,
  /**
   * @brief The @p C coordinate index
   */
  abc_c = 2,
};

/**
 * @brief The @p ABC vector data
 */
#define abc_t(type) union {    \
    struct {                   \
      /**                      \
       * @brief The @p a value \
       */                      \
      type a;                  \
      /**                      \
       * @brief The @p b value \
       */                      \
      type b;                  \
      /**                      \
       * @brief The @p c value \
       */                      \
      type c;                  \
    };                         \
    /**                        \
     * @brief The values array \
     */                        \
    type _[3];                 \
  }

/**
 * @brief The @p DQ indexes
 */
enum {
  /**
   * @brief The @p D coordinate index
   */
  dq_d = 0,
  /**
   * @brief The @p Q coordinate index
   */
  dq_q = 1,
};

/**
 * @brief The @p DQ vector data
 */
#define dq_t(type) union {     \
    struct {                   \
      /**                      \
       * @brief The @p D value \
       */                      \
      type d;                  \
      /**                      \
       * @brief The @p Q value \
       */                      \
      type q;                  \
    };                         \
    /**                        \
     * @brief The values array \
     */                        \
    type _[2];                 \
  }

/**
 * @}
 */
#endif /* __CTL__TYPE_H__ */

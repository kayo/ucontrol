#ifndef __CTL__MED_H__
#define __CTL__MED_H__
/**
 * @ingroup filters
 * @defgroup med Median filter
 * @brief This part implements median filtering.
 *
 * This filter much effective for random noise suppression in source signal.
 *
 * @{
 */

#include <ctl/type.h>
#include <ctl/vec.h>

/**
 * @brief Evaluate filtering step
 *
 * @param[in] type The value type to operate
 * @param[in] data The pointer to data array
 * @param[in] len The number of all values
 * @param[in] num The number of median values
 * @param[in] ... The arguments for accessor
 * @return The filtered value
 *
 * The result of filtering calculated as average of @p num middle values.
 * If the parity of @p num mismatched parity of @p len, then the number of middle values will be incremented by one.
 *
 * @note This filter has a side-effects: it changes the order of values in data array. To avoid it you can copying data first.
 */
#define med_step(type, data, len, num, ...) ({  \
      vec_sort(type, vec_sort_asc, data, len,   \
               ##__VA_ARGS__);                  \
                                                \
      uint16_t cnt = (num) < (len) ?            \
        (((len) & 0x1) == ((num) & 0x1) ?       \
         (num) : (num) + 1) : (len);            \
      type wht = gen_inv(type,                  \
                         gen_make(type, cnt));  \
      uint16_t beg = ((len) >> 1) - (cnt >> 1); \
      uint16_t end = beg + cnt;                 \
      uint16_t idx;                             \
                                                \
      type res = gen_0(type);                   \
      for (idx = beg; idx < end; idx ++) {      \
        res = gen_add(type, res,                \
                      gen_mul(type, wht,        \
                      vec_pick(data, idx,       \
                            ##__VA_ARGS__)));   \
      }                                         \
                                                \
      res;                                      \
    })

/**
 * @}
 */
#endif /* __CTL__MED_H__ */





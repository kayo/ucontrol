#ifndef __CTL__AB_H__
#define __CTL__AB_H__
/**
 * @ingroup converters
 * @defgroup ab αβ coordinate transformations
 *
 * @brief This module implements αβ transformations (also known as Clarke transformations).
 * @{
 */

#include <ctl/type.h>

/**
 * @brief Convert ABC coords to αβ coords
 *
 * The direct Clarke transformation
 *
 * @param[in] type The scalar type to operate
 * @param[in] abc The pointer to @p ABC vector values
 * @param[out] ab The pointer to @p αβ vector values
 */
#define abc_to_ab(type, abc, ab) {                    \
    /* α = a */                                       \
    (ab)->a = (abc)->a;                               \
                                                      \
    /* β = (a + 2 * b) / sqrt(3) */                   \
    (ab)->b = gen_mul(type,                           \
                      gen_add(type,                   \
                              (abc)->a,               \
                              gen_mul(type,           \
                                      (abc)->b,       \
                                      gen_2(type))),  \
                      gen_inv_sqrt_3(type));          \
  }

/**
 * @brief Convert αβ coords to ABC coords
 *
 * The inverted Clarke transformation
 *
 * @param[in] type The scalar type to operate
 * @param[in] dq The pointer to @p αβ vector values
 * @param[out] abc The pointer to @p ABC vector values
 */
#define ab_to_abc(type, ab, abc) {                \
    /* a = α */                                   \
    (abc)->a = (ab)->a;                           \
                                                  \
    type half_a = gen_mul(type, (ab)->a,          \
                          gen_inv_2(type));       \
                                                  \
    type half_sqrt_3_b =                          \
      gen_mul(type, (ab)->b,                      \
              gen_sqrt_3_inv_2(type));            \
                                                  \
    /* b = (sqrt(3) * β - α) / 2 */               \
    (abc)->b = gen_sub(type,                      \
                       half_sqrt_3_b, half_a);    \
                                                  \
    /* c = (-sqrt(3) * β - α) / 2 */              \
    (abc)->c = gen_sub(type,                      \
                       gen_neg(type,              \
                               half_sqrt_3_b),    \
                       half_a);                   \
  }

/**
 * @}
 */
#endif /* __CTL__AB_H__ */

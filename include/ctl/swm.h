#ifndef __CTL__SWM_H__
#define __CTL__SWM_H__
/**
 * @ingroup converters
 * @defgroup swm Sine-wave 3-phase modulation
 * @brief This module implements classic sine-wave 3-phase modulation
 *
 * @{
 */

#include <ctl/type.h>

/**
 * @brief The internal state of modulator
 */
#define swm_t(type) struct {                    \
    /**                                         \
     * @brief The actual magnetic field angle   \
     */                                         \
    type angle;                                 \
  }

/**
 * @brief Initialize modulator state
 *
 * @param[in] type The scalar type to operate
 * @param[out] state The pointer to modulator state
 */
#define swm_init(type, state) {    \
    (state)->angle = gen_0(type);  \
  }

/**
 * @brief Run calculation of duty cycle values
 *
 * You must call this function synchronous with control frequency.
 *
 * @param[in] type The scalar type to operate
 * @param[in] mf_step The magnitude-frequency step
 * @param[in,out] state The pointer to modulator state
 * @param[out] abc The pointer to modulator output
 */
#define swm_step(type, mf_step, state, abc) {           \
    /* angle += step */                                 \
    (state)->angle = gen_add(type, (state)->angle,      \
                             (mf_step));                \
                                                        \
    /* angle >= 2 * PI */                               \
    if (!gen_lt(type, (state)->angle,                   \
                gen_pi_2(type))) {                      \
      /* angle -= 2 * PI */                             \
      (state)->angle = gen_sub(type, (state)->angle,    \
                               gen_pi_2(type));         \
    }                                                   \
                                                        \
    /* a = sin(angle) */                                \
    (abc)->a = gen_sin(type, (state)->angle);           \
    /* b = sin(angle + PI * 2 / 3) */                   \
    (abc)->b = gen_sin(type,                            \
                       gen_add(type, (state)->angle,    \
                               gen_pi_2_inv_3(type)));  \
    /* c = sin(angle - PI * 2 / 3) */                   \
    (abc)->c = gen_sin(type,                            \
                       gen_sub(type, (state)->angle,    \
                               gen_pi_2_inv_3(type)));  \
  }

/**
 * @}
 */
#endif /* __CTL__SWM_H__ */

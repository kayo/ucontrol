#ifndef __CTL__DEBUG_H__
#define __CTL__DEBUG_H__

#ifndef ctl_assert
#ifdef CTL_ASSERT
#include <assert.h>
#define ctl_assert(exp, fmt, ...) {        \
    if (!(exp)) {                          \
      fprintf(stderr, "[ucontrol assert] " \
              fmt "\n", ##__VA_ARGS__);    \
      assert(exp);                         \
    }                                      \
  }
#else /* !CTL_ASSERT */
#define ctl_assert(...)
#endif /* CTL_ASSERT */
#endif /* ctl_assert */

#ifndef ctl_debug
#ifdef CTL_DEBUG
#include <stdio.h>
#define ctl_debug(fmt, ...)           \
  fprintf(stderr, "[ucontrol debug] " \
          fmt "\n", ##__VA_ARGS__)
#else /* CTL_DEBUG */
#define ctl_debug(...)
#endif /* CTL_DEBUG */
#endif /* ctl_debug */

#endif /* __CTL__DEBUG_H__ */

#ifndef __CTL__DIF_H__
#define __CTL__DIF_H__
/**
 * @ingroup convertors
 * @defgroup dif Derivation of value
 * @brief This part implements derivative of value
 *
 * @{
 */

#include <ctl/type.h>

/**
 * @brief Derivation parameters type
 *
 * @param[in] type The type of values
 * @return The derivative parameters structure
 */
#define dif_param_t(type)                       \
  struct {                                      \
    /**                                         \
     * @brief Derivation factor (frequency)     \
     */                                         \
    type F;                                     \
  }

/**
 * @brief Initialize derivation parameters
 *
 * @param[in] type The type of values
 * @param[in] period The sampling time (control step period)
 * @return The derivation parameter.
 */
#define dif_param(type, period) {  \
    gen_inv(type, period)          \
  }

/**
 * @brief Set step period
 *
 * @param[in] type The type of values
 * @param[in,out] param The pointer to derivation parameters
 * @param[in] period The sampling time (control step period)
 */
#define dif_set_period(type, param, period)  \
  (param)->F = gen_inv(type, period)

/**
 * @brief Derivative state type
 *
 * @param[in] type The type of values
 * @return The derivative state structure
 */
#define dif_state_t(type)                               \
  struct {                                              \
    /**                                                 \
     * @brief Last value                                \
     *                                                  \
     * The previous value.                              \
     */                                                 \
    type V;                                             \
  }

/**
 * @brief Initialize derivation state
 *
 * @param[in] type The type of values
 * @param[in,out] state The pointer to state
 * @param[in] value The initial value
 */
#define dif_init(type, state, value) {    \
    (state)->V = (value);                 \
  }

/**
 * @brief Evaluate derivation step
 *
 * @param[in] type The type of values
 * @param[in] param The pointer to PID parameters
 * @param[in,out] state The pointer to PID state
 * @param[in] value The actual value
 * @return The result derivation
 */
#define dif_step(type, param, state, value) ({  \
      type _V = (state)->V;                     \
      (state)->V = (value);                     \
      gen_mul(type,                             \
              (param)->F,                       \
              gen_sub(type, (state)->V, _V));   \
    })

/**
 * @}
 */
#endif /* __CTL__DIF_H__ */

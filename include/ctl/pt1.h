#ifndef __CTL__PT1_H__
#define __CTL__PT1_H__
/**
 * @ingroup filters
 * @defgroup pt1 PT1 filter
 * @brief This part implements proportional transmission behavior filtering with delay of the first order (PT1).
 *
 * @{
 */

#include <ctl/type.h>

/**
 * @brief PT1 parameters type
 *
 * @param[in] type The type of values
 * @return The parameters structure
 */
#define pt1_param_t(type)                \
  struct {                               \
    /**                                  \
     * @brief The time factor of filter  \
     *                                   \
     * 1.0 / (1.0 + Tf/Ts)               \
     */                                  \
    type Kx;                             \
    /**                                  \
     * @brief The time factor of filter  \
     *                                   \
     * (Tf/Ts) / (1.0 + Tf/Ts)           \
     */                                  \
    type _Kx;                            \
  }

/**
 * @brief Set time factor of filter
 *
 * @param[in] type The type of values
 * @param[in,out] param The pointer to filter parameters
 * @param[in] time The time factor value
 * @param[in] period The sampling time (step period)
 */
#define pt1_set_T(type, param, time, period)       \
  ({                                               \
    const type T = gen_div(type, time, period);    \
    const type T1 = gen_add(type, T, gen_1(type)); \
                                                   \
    (param)->Kx = gen_div(type, gen_1(type), T1);  \
    (param)->_Kx = gen_div(type, T, T1);           \
    T;                                             \
  })

/**
 * @brief PT1 state type
 *
 * @param[in] type The type of values
 * @return The state structure
 */
#define pt1_state_t(type)                  \
  struct {                                 \
    /**                                    \
     * @brief The previous filtered value  \
     */                                    \
    type X;                                \
  }

/**
 * @brief Initialize filter state
 *
 * @param[in] type The type of values
 * @param[in,out] state The pointer to filter state
 * @param[in] value The initial source value
 */
#define pt1_init(type, state, value)            \
  ((state)->X = (value))

/**
 * @brief Evaluate filtering step
 *
 * @param[in] type The type of values
 * @param[in] param The pointer to filter parameters
 * @param[in,out] state The pointer to filter state
 * @param[in] value The source value
 * @return The filtered value
 */
#define pt1_step(type, param, state, value)      \
  /* state.X = (1.0 * X + (param.T * state.X))   \
     / (1.0 + param.T)                           \
     state.X = X * (1.0 / (1.0 + param.T))       \
     + state.X * (param.T / (1.0 + param.T)) */  \
  ((state)->X =                                  \
   gen_add(type,                                 \
           gen_mul(type, (value),                \
                   (param)->Kx),                 \
           gen_mul(type, (state)->X,             \
                   (param)->_Kx)))

/**
 * @}
 */
#endif /* __CTL__PT1_H__ */

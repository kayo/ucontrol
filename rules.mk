control.BASEPATH := $(subst $(dir $(abspath $(CURDIR)/xyz)),,$(dir $(abspath $(lastword $(MAKEFILE_LIST)))))
control.TDIRS := $(control.BASEPATH)src

# library, config path -> file path
control.GEN_P = gen/$(1)/uctl/$(notdir $(basename $(2))).h
control.AG = GUILE_AUTO_COMPILE=0 autogen $(addprefix -L,$(control.TDIRS)) -T$(1) $(2)$(if $(3), > $(3))

# library, config
define CTL_RULE
ifndef control.$(1).$(2)
control.$(1).$(2) := $(call control.GEN_P,$(1),$(2))
$$(control.$(1).$(2)): $(2)
	@echo TARGET $(1) CONTROL $(2) GEN
	$(Q)mkdir -p $$(dir $$@)
	$(Q)$$(call control.AG,control,$$<,$$@)
$(1).CDIRS += $$(dir $$(control.$(1).$(2)))
build.control: $$(control.$(1).$(2))
$$($(1).SRCS): $$(control.$(1).$(2))
clean: clean.control.$(1).$(2)
clean.control.$(1).$(2):
	@echo TARGET $(1) CONTROL $(2) CLEAN
	$(Q)rm -f $$(control.$(1).$(2))
endif
endef

# library
define CTL_RULES
ifneq (,$($(1).CTLS))
$(foreach f,$($(1).CTLS),
$(call CTL_RULE,$(1),$(f)))
endif
endef

TARGET.LIBS += libcontrol
libcontrol.INHERIT ?= firmware
libcontrol.CDIRS := $(control.BASEPATH)include
libcontrol.SRCS := $(control.BASEPATH)src/type.c

TARGET.CTLS += libcontrol
libcontrol.CTLS := $(control.BASEPATH)src/lookup.def

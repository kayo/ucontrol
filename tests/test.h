#include <stdio.h>

#define warn(fmt, ...) fprintf(stderr, fmt "\n", ##__VA_ARGS__)
#define info(fmt, ...) fprintf(stdout, fmt "\n", ##__VA_ARGS__)

#define gnuplot(plots, datas) {                   \
    FILE *g = popen("gnuplot", "w");              \
    fprintf(g, "plot " plots "\n");               \
    datas;                                        \
    fprintf(g, "pause mouse key\n");              \
    pclose(g);                                    \
  }

#define plotstr1(title)                           \
  "'-' using 1:2 with steps title '" title "', "

#define plotfun1(type, fun, from, to, steps) {      \
    const type f = gen_make(type, from);            \
    const type t = gen_make(type, to);              \
    const type s = gen_div(type,                    \
                           gen_sub(type, t, f),     \
                           gen_make(type, steps));  \
    type a;                                         \
    for (a = f; a < t; a = gen_add(type, a, s)) {   \
      fprintf(g, "%f %f\n", gen_float(type, a),     \
              gen_float(type, fun(a)));             \
    }                                               \
    fprintf(g, "e\n");                              \
  }

#define plotrun(init, step, print, steps) {         \
    unsigned i = 0;                                 \
    init;                                           \
    for (; i < steps; i++) {                        \
      step;                                         \
      print;                                        \
    }                                               \
    fprintf(g, "e\n");                              \
  }

#define plotrun1(type, init, step, arg, res, steps) \
  plotrun(init, step,                               \
          fprintf(g, "%f %f\n",                     \
                  gen_float(type, arg),             \
                  gen_float(type, res)),            \
          steps)

#define plotrun2(type, init, step, arg1, res1, arg2, res2, steps)    \
  plotrun(init, step,                                                \
          fprintf(g, "%f %f\n",                                      \
                  gen_float(type, arg1), gen_float(type, res1));     \
          fprintf(g, "%f %f\n",                                      \
                  gen_float(type, arg2), gen_float(type, res2)),     \
          steps)

#define addxgrid(fmt, arg...)                                 \
  fprintf(g, "set xtics " fmt "; set grid xtics;\n", ##arg);

#define addxgrid1(type, incr)                 \
  addxgrid("%f", gen_float(type, incr))

#define addxgrid2(type, start, incr)          \
  addxgrid("%f, %f", gen_float(type, start),  \
           gen_float(type, incr));

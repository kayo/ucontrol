#include <ctl/dq.h>
#include <ctl/util.h>
#include "test.h"

#define test_dq(type) {                             \
    type step = mf_step(type,                       \
                        gen_make(type, period),     \
                        gen_make(type, frequency)); \
    info(#type " mf_step: %f",                      \
         gen_float(type, step));                    \
                                                    \
    ab_t(type) ab, ab_;                             \
    dq_t(type) dq;                                  \
                                                    \
    type angle = gen_0(type);                       \
    int i;                                          \
    for (i = 0; i < 10; i++) {                      \
      angle = gen_add(type, angle, step);           \
                                                    \
      ab.a = gen_sin(type, angle);                  \
      ab.b = gen_cos(type, angle);                  \
                                                    \
      ab_to_dq(type, &ab, angle, &dq);              \
      dq_to_ab(type, &dq, angle, &ab_);             \
                                                    \
      info("%f [%f, %f] [%f, %f] [%f, %f]",         \
           gen_float(type, angle),                  \
           gen_float(type, ab.a),                   \
           gen_float(type, ab.b),                   \
           gen_float(type, dq.d),                   \
           gen_float(type, dq.q),                   \
           gen_float(type, ab_.a),                  \
           gen_float(type, ab_.b));                 \
    }                                               \
  }

int main(void) {
#define period 10e-3
#define frequency 10

  test_dq(float);
  test_dq(fix8);
  test_dq(fix16);
  test_dq(fix24);

  return 0;
}

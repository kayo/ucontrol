#include <ctl/util.h>
#include <ctl/pid.h>
#include "test.h"

#define test_pid(type) {                                \
    pid_param_t(type) pid_param;                        \
                                                        \
    pid_set_Kp(type, &pid_param, gen_make(type, 0.5));  \
    pid_set_Ti(type, &pid_param, gen_make(type, 1.0),   \
               gen_make(type, step));                   \
    pid_set_Td(type, &pid_param,                        \
               gen_make(type, 0.7),                     \
               gen_make(type, step));                   \
                                                        \
    pid_state_t(type) pid_state;                        \
    pid_init(type, &pid_state);                         \
                                                        \
    type value = gen_make(type, 0.123456);              \
    type result;                                        \
                                                        \
    for (; gen_lt(type, value,                          \
                  gen_make(type, 30.0));                \
         value = gen_add(type, value,                   \
                         gen_make(type, 0.1))) {        \
      result = pid_step(type, &pid_param, &pid_state,   \
                        error_val(type,                 \
                                  gen_make(type, 12.0), \
                                  value));              \
    }                                                   \
                                                        \
    info(#type ": %f", gen_float(type, result));        \
  }

int main(void) {
#define step 10e-3
  
  test_pid(float);
  test_pid(fix8);
  test_pid(fix16);
  test_pid(fix24);

  return 0;
}

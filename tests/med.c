#include <ctl/med.h>
#include "test.h"

int main(void) {
  struct {
    fix16 a;
    fix8 b;
  } buf[5] = {
    { gen_make(fix16, 11.0), gen_make(fix24, 1.0) },
    { gen_make(fix16, 1.0), gen_make(fix24, -10.0) },
    { gen_make(fix16, -5.0), gen_make(fix24, 3.14) },
    { gen_make(fix16, 0.01), gen_make(fix24, 108.0) },
    { gen_make(fix16, -0.501), gen_make(fix24, -3.1) },
  };

  info("med .a 4, 1: %f", gen_float(fix16, med_step(fix16, buf, 4, 1, .a)));
  info("med .a 4, 2: %f", gen_float(fix16, med_step(fix16, buf, 4, 2, .a)));
  info("med .a 4, 3: %f", gen_float(fix16, med_step(fix16, buf, 4, 3, .a)));
  info("med .a 5, 1: %f", gen_float(fix16, med_step(fix16, buf, 5, 1, .a)));
  info("med .a 5, 2: %f", gen_float(fix16, med_step(fix16, buf, 5, 2, .a)));
  info("med .a 5, 3: %f", gen_float(fix16, med_step(fix16, buf, 5, 3, .a)));
  info("med .a 5, 4: %f", gen_float(fix16, med_step(fix16, buf, 5, 4, .a)));
  
  return 0;
}

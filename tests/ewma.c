#include <ctl/ewma.h>
#include "test.h"

float data[] = {
  0.123456,
  1.01246,
  5.198,
  4.0,
  2.7643,
  -0.0124,
  -1.2984,
  0.7633,
};

#define test_ewma(type) {                       \
    ewma_param_t(type) ewma_param;              \
                                                \
    ewma_set_T(type, &ewma_param,               \
               gen_make(type, time),            \
               gen_make(type, step));           \
                                                \
    ewma_state_t(type) ewma_state;              \
    ewma_init(type, &ewma_state,                \
              gen_make(type, data[0]));         \
                                                \
    type result;                                \
    unsigned i;                                 \
                                                \
    for (i = 1;                                 \
         i < sizeof(data)/sizeof(data[0]);      \
         i++) {                                 \
      result = ewma_step(type, &ewma_param,     \
                         &ewma_state,           \
                         gen_make(type,         \
                                  data[i]));    \
    }                                           \
                                                \
    info(#type ": %f", gen_float(type,          \
                                 result));      \
  }

int main(void) {
#define time 100e-3
#define step 10e-3
  
  test_ewma(float);
  test_ewma(fix8);
  test_ewma(fix16);
  test_ewma(fix24);
  
  return 0;
}

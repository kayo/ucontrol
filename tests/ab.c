#include <ctl/ab.h>
#include <ctl/swm.h>
#include <ctl/util.h>
#include "test.h"

#define test_ab(type) {                             \
    swm_t(type) swm;                                \
    swm_init(type, &swm);                           \
                                                    \
    type step = mf_step(type,                       \
                        gen_make(type, period),     \
                        gen_make(type, frequency)); \
    info(#type " mf_step: %f",                      \
         gen_float(type, step));                    \
                                                    \
    abc_t(type) abc, abc_;                          \
    ab_t(type) ab;                                  \
                                                    \
    int i;                                          \
    for (i = 0; i < 10; i++) {                      \
      swm_step(type, step, &swm, &abc);             \
      abc_to_ab(type, &abc, &ab);                   \
      ab_to_abc(type, &ab, &abc_);                  \
                                                    \
      info("%f [%f, %f, %f] [%f, %f] [%f, %f, %f]", \
           gen_float(type, swm.angle),              \
           gen_float(type, abc.a),                  \
           gen_float(type, abc.b),                  \
           gen_float(type, abc.c),                  \
           gen_float(type, ab.a),                   \
           gen_float(type, ab.b),                   \
           gen_float(type, abc_.a),                 \
           gen_float(type, abc_.b),                 \
           gen_float(type, abc_.c));                \
    }                                               \
  }

int main(void) {
#define period 10e-3
#define frequency 10

  test_ab(float);
  test_ab(fix8);
  test_ab(fix16);
  test_ab(fix24);

  return 0;
}

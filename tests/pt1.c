#include <ctl/pt1.h>
#include "test.h"

float data[] = {
  0.123456,
  1.01246,
  5.198,
  4.0,
  2.7643,
  -0.0124,
  -1.2984,
  0.7633,
};

#define test_pt1(type) {                        \
    pt1_param_t(type) pt1_param;                \
                                                \
    pt1_set_T(type, &pt1_param,                 \
              gen_make(type, time),             \
              gen_make(type, step));            \
                                                \
    pt1_state_t(type) pt1_state;                \
    pt1_init(type, &pt1_state,                  \
             gen_make(type, data[0]));          \
                                                \
    type result;                                \
    unsigned i;                                 \
                                                \
    for (i = 1;                                 \
         i < sizeof(data)/sizeof(data[0]);      \
         i++) {                                 \
      result = pt1_step(type, &pt1_param,       \
                        &pt1_state,             \
                        gen_make(type,          \
                                 data[i]));     \
    }                                           \
                                                \
    info(#type ": %f", gen_float(type,          \
                                 result));      \
  }

int main(void) {
#define time 100e-3
#define step 10e-3
  
  test_pt1(float);
  test_pt1(fix8);
  test_pt1(fix16);
  test_pt1(fix24);
  
  return 0;
}

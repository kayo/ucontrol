#include <ctl/fir.h>
#include "fir.h"
#include "test.h"

float data[] = {
  0.1,
  -0.1,
  0.52,
  0.79,
  2.7,
  1.3,
  -1.6,
};

#define fir_win(type, window)       \
  gen_float(type,                   \
    fir_step(type,                  \
             fir_##type##_##window, \
             &dl))

#define test_fir(type) {                                \
    dl_t(type, depth) dl;                               \
                                                        \
    int i;                                              \
    for (i = 0; i < depth; i++) {                       \
      dl_push(&dl,                                      \
              gen_make(type, data[i]));                 \
    }                                                   \
                                                        \
    info(#type " "                                      \
         "rct=%f "                                      \
         "tri=%f "                                      \
         "prz=%f "                                      \
         "wlch=%f "                                     \
         "sin=%f "                                      \
         "han=%f "                                      \
         "ham=%f "                                      \
         "ham0=%f "                                     \
         "bkmn=%f "                                     \
         "ntal=%f "                                     \
         "bmnl=%f "                                     \
         "bmhr=%f "                                     \
         "ft=%f "                                       \
         "rv2=%f",                                      \
         fir_win(type, rectangular),                    \
         fir_win(type, triangular),                     \
         fir_win(type, parzen),                         \
         fir_win(type, welch),                          \
         fir_win(type, sine),                           \
         fir_win(type, hann),                           \
         fir_win(type, hamming),                        \
         fir_win(type, hamming0),                       \
         fir_win(type, blackman),                       \
         fir_win(type, nuttall),                        \
         fir_win(type, blackman_nuttall),               \
         fir_win(type, blackman_harris),                \
         fir_win(type, flat_top),                       \
         fir_win(type, rife_vincent2));                 \
  }

int main(void) {
#define depth 7
  
  test_fir(float);
  test_fir(fix8);
  test_fir(fix16);
  test_fir(fix24);

  return 0;
}

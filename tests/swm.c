#include <ctl/swm.h>
#include <ctl/util.h>
#include "test.h"

#define plotpwm(type, var)                          \
  plotrun1(type, swm_init(type, &swm_state),        \
           swm_step(type, step, &swm_state, &abc),  \
           gen_mul(type, step, gen_make(type, i)),  \
           var, 100)
  
#define plotpwmT(type, var)                           \
  plotpwm(type,                                       \
          gen_add(type, abc.var, gen_make(type, 0)))
#define plotpwmU(type, var1, var2)                    \
  plotpwm(type,                                       \
          gen_sub(type,                               \
                  gen_sub(type, abc.var2, abc.var1),  \
                  gen_3(type)))
  
#define plotpwmM(type, var, num)                    \
  plotrun1(type, char b = 1;                        \
           swm_init(type, &swm_state);              \
           fprintf(g, "%f %f\n", 0.0,               \
                   1.5 - 0.15 * num),               \
           b = !b; swm_step(type, step,             \
                            &swm_state, &abc),      \
           gen_mul(type, step, b & 1 ?              \
                   gen_add(type,                    \
                           gen_make(type, i),       \
                           abc.var) :               \
                   gen_sub(type,                    \
                           gen_make(type, i + 1),   \
                           abc.var)),               \
           gen_make(type, !b * 0.1 +                \
                    1.5 - 0.15 * num), 100)

#define plot_swm(type) {                            \
    type step = mf_step(type,                       \
                        gen_make(type, period),     \
                        gen_make(type, frequency)); \
    info(#type " mf_step: %f",                      \
         gen_float(type, step));                    \
                                                    \
    swm_t(type) swm_state;                          \
    abc_t(type) abc;                                \
                                                    \
    gnuplot(plotstr1("Ta")                          \
            plotstr1("Tb")                          \
            plotstr1("Tc")                          \
                                                    \
            plotstr1("Tab-3")                       \
            plotstr1("Tbc-3")                       \
            plotstr1("Tca-3")                       \
                                                    \
            plotstr1("Ua")                          \
            plotstr1("Ub")                          \
            plotstr1("Uc"),                         \
                                                    \
            plotpwmT(type, a)                       \
            plotpwmT(type, b)                       \
            plotpwmT(type, c)                       \
                                                    \
            plotpwmU(type, a, b)                    \
            plotpwmU(type, b, c)                    \
            plotpwmU(type, c, a)                    \
                                                    \
            plotpwmM(type, a, 1)                    \
            plotpwmM(type, b, 2)                    \
            plotpwmM(type, c, 3)                    \
                                                    \
            addxgrid1(type, step));                 \
  }

#define test_swm(type) {                            \
    type step = mf_step(type,                       \
                        gen_make(type, period),     \
                        gen_make(type, frequency)); \
    info(#type " mf_step: %f",                      \
         gen_float(type, step));                    \
                                                    \
    swm_t(type) swm_state;                          \
    swm_init(type, &swm_state);                     \
                                                    \
    abc_t(type) abc;                                \
                                                    \
    int i;                                          \
    for (i = 0; i < 10; i++) {                      \
      swm_step(type, step, &swm_state, &abc);       \
      info("%f [%f, %f, %f]",                       \
           gen_float(type, swm_state.angle),        \
           gen_float(type, abc.a),                  \
           gen_float(type, abc.b),                  \
           gen_float(type, abc.c));                 \
    }                                               \
  }

int main(void) {
#define period 10e-3
#define frequency 10
  
  test_swm(float);
  test_swm(fix8);
  test_swm(fix16);
  test_swm(fix24);

#undef period
#undef frequency

#define period 250e-6
#define frequency 50

  plot_swm(float);
  plot_swm(fix16);
  plot_swm(fix24);

  return 0;
}

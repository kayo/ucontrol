#include <ctl/type.h>
#include <ctl/util.h>
#include "test.h"

#define print_table(type) {                     \
    type x = t_from;                            \
    int i = 0;                                  \
                                                \
    for (; i < lenof(t_data); i++) {            \
      info("  [%f] => %f",                      \
           gen_float(type, x),                  \
           gen_float(type, t_data[i]));         \
      x = gen_add(type, x, t_step);             \
    }                                           \
  }
  

#define test_lookup_var(type, from, step, _0, _1, _2, _3, _4) \
  info("lookup table (" #type "):");                          \
  const type t_from = gen_make(type, from);                   \
  const type t_step = gen_make(type, step);                   \
  const type t_data[] = {                                     \
    gen_make(type, _0),                                       \
    gen_make(type, _1),                                       \
    gen_make(type, _2),                                       \
    gen_make(type, _3),                                       \
    gen_make(type, _4),                                       \
  };                                                          \
  print_table(type);                                          \
  info("test lookup:")

#define test_lookup_val(type, val)                  \
  info("  [%f] => %f", val,                         \
       gen_float(type,                              \
                 lookup_val(type, t,                \
                            gen_make(type, val))))  \

#define test_lookup_arg(type, arg)                  \
  info("  [%f] <= %f",                              \
       gen_float(type,                              \
                 lookup_arg(type, t,                \
                            gen_make(type, arg))),  \
       arg)                                         \

#define test_lookup_1(type) {                     \
    test_lookup_var(type, 0.3, 0.1,               \
                    0.1, 0.2, 0.4, 0.8, 0.9);     \
                                                  \
    test_lookup_val(type, -0.1);                  \
    test_lookup_val(type, 0.2);                   \
    test_lookup_val(type, 0.3);                   \
    test_lookup_val(type, 0.35);                  \
    test_lookup_val(type, 0.58);                  \
    test_lookup_val(type, 0.9);                   \
    test_lookup_val(type, 1.0);                   \
                                                  \
    test_lookup_arg(type, -0.3);                  \
    test_lookup_arg(type, 0.0);                   \
    test_lookup_arg(type, 0.1);                   \
    test_lookup_arg(type, 0.15);                  \
    test_lookup_arg(type, 0.72);                  \
    test_lookup_arg(type, 1.1);                   \
    test_lookup_arg(type, 1.2);                   \
  }

#define test_lookup_2(type) {                     \
    test_lookup_var(type, -0.1, 0.2,              \
                    0.9, 0.8, 0.4, 0.2, -0.1);    \
                                                  \
    test_lookup_val(type, -0.2);                  \
    test_lookup_val(type, 0.0);                   \
    test_lookup_val(type, 0.1);                   \
    test_lookup_val(type, 0.33);                  \
    test_lookup_val(type, 0.57);                  \
    test_lookup_val(type, 0.9);                   \
    test_lookup_val(type, 1.0);                   \
                                                  \
    test_lookup_arg(type, 0.95);                  \
    test_lookup_arg(type, 0.85);                  \
    test_lookup_arg(type, 0.8);                   \
    test_lookup_arg(type, 0.37);                  \
    test_lookup_arg(type, 0.095);                 \
    test_lookup_arg(type, -0.4);                  \
    test_lookup_arg(type, -0.55);                 \
  }

#define test_lookup(type) \
  test_lookup_1(type);    \
  test_lookup_2(type);

int main() {
  test_lookup(fix8);
  test_lookup(fix16);
  test_lookup(fix24);
  
  return 0;
}

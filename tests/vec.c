#include <ctl/type.h>
#include <ctl/vec.h>
#include "test.h"

int main(void) {
  struct {
    fix16 a;
    fix24 b;
  } v[5] = {
    { gen_make(fix16, 11.0), gen_make(fix24, 1.0) },
    { gen_make(fix16, 1.0), gen_make(fix24, -10.0) },
    { gen_make(fix16, -5.0), gen_make(fix24, 3.14) },
    { gen_make(fix16, 0.01), gen_make(fix24, 108.0) },
    { gen_make(fix16, -0.501), gen_make(fix24, -3.1) },
  };

  info("pick [0]->a: %f", gen_float(fix16, vec_pick(v, 0, .a)));
  info("pick [2]->a: %f", gen_float(fix16, vec_pick(v, 2, .a)));

  info("swap [0]->a, [2]->a");
  vec_swap(v, 0, 2, .a);
  
  info("pick [0]->a: %f", gen_float(fix16, vec_pick(v, 0, .a)));
  info("pick [2]->a: %f", gen_float(fix16, vec_pick(v, 2, .a)));

  info("sort [x]->a");
  vec_sort(fix16, vec_sort_asc, v, vec_size(v), .a);
  
  info("pick [0]->a: %f", gen_float(fix16, vec_pick(v, 0, .a)));
  info("pick [1]->a: %f", gen_float(fix16, vec_pick(v, 1, .a)));
  info("pick [2]->a: %f", gen_float(fix16, vec_pick(v, 2, .a)));
  info("pick [3]->a: %f", gen_float(fix16, vec_pick(v, 3, .a)));
  info("pick [4]->a: %f", gen_float(fix16, vec_pick(v, 4, .a)));

  info("sort [x]->b");
  vec_sort(fix24, vec_sort_desc, v, vec_size(v), .b);
  
  info("pick [0]->b: %f", gen_float(fix24, vec_pick(v, 0, .b)));
  info("pick [1]->b: %f", gen_float(fix24, vec_pick(v, 1, .b)));
  info("pick [2]->b: %f", gen_float(fix24, vec_pick(v, 2, .b)));
  info("pick [3]->b: %f", gen_float(fix24, vec_pick(v, 3, .b)));
  info("pick [4]->b: %f", gen_float(fix24, vec_pick(v, 4, .b)));
  
  return 0;
}

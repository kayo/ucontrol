#include <ctl/type.h>
#include "test.h"

#define show_cast(type, rtype, arg)                 \
  info("  cast<" #rtype ">(" #arg ") = %f",         \
       gen_float(rtype,                             \
                 gen_to(type, rtype,                \
                        gen_make(type, arg))))

#define test_cast(type, rtype)         \
  show_cast(type, rtype, 0.0);         \
  show_cast(type, rtype, 1.0);         \
  show_cast(type, rtype, -1.0);        \
  show_cast(type, rtype, 0.1);         \
  show_cast(type, rtype, -0.1);        \
  show_cast(type, rtype, 0.123456789)

#define show_trig(type, func, arg)                  \
  info("  " #func "(" #arg ") = %f",                \
       gen_float(type,                              \
                 gen_##func(type,                   \
                            gen_make(type, arg))))

#define test_trig(type, func) {        \
    show_trig(type, func, 0);          \
    show_trig(type, func, M_PI/2);     \
    show_trig(type, func, M_PI/3);     \
    show_trig(type, func, M_PI/4);     \
    show_trig(type, func, M_PI/6);     \
    show_trig(type, func, -M_PI/2);    \
    show_trig(type, func, -M_PI/3);    \
    show_trig(type, func, -M_PI/4);    \
    show_trig(type, func, -M_PI/6);    \
    show_trig(type, func, M_PI*2/3);   \
    show_trig(type, func, M_PI*3/4);   \
    show_trig(type, func, M_PI*5/6);   \
    show_trig(type, func, -M_PI*2/3);  \
    show_trig(type, func, -M_PI*3/4);  \
    show_trig(type, func, -M_PI*5/6);  \
    show_trig(type, func, M_PI);       \
    show_trig(type, func, 2*M_PI);     \
    show_trig(type, func, 3*M_PI);     \
    show_trig(type, func, 4*M_PI);     \
    show_trig(type, func, -M_PI);      \
    show_trig(type, func, -2*M_PI);    \
    show_trig(type, func, -3*M_PI);    \
    show_trig(type, func, -4*M_PI);    \
  }

#define test_type(type) {              \
    info(#type ":");                   \
    test_cast(type, float);            \
    test_cast(type, fix8);             \
    test_cast(type, fix16);            \
    test_cast(type, fix24);            \
    test_trig(type, sin);              \
    test_trig(type, cos);              \
  }

int main(void) {
  test_type(float);
  test_type(fix8);
  test_type(fix16);
  test_type(fix24);
  
  return 0;
}

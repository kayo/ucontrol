#include <ctl/lqe.h>
#include "test.h"

float data[] = {
  0.123456,
  1.01246,
  5.198,
  4.0,
  2.7643,
  -0.0124,
  -1.2984,
  0.7633,
};

#define test_lqe(type) {                        \
    lqe_param_t(type) lqe_param;                \
                                                \
    lqe_set_F(type, &lqe_param,                 \
              gen_make(type, 0.6));             \
                                                \
    lqe_set_H(type, &lqe_param,                 \
              gen_make(type, 0.5));             \
                                                \
    lqe_set_Q(type, &lqe_param,                 \
              gen_make(type, 0.2));             \
                                                \
    lqe_set_R(type, &lqe_param,                 \
              gen_make(type, 0.4));             \
                                                \
    lqe_state_t(type) lqe_state;                \
    lqe_init(type, &lqe_state,                  \
             gen_make(type, data[0]),           \
             gen_make(type, 1.0));              \
                                                \
    type result;                                \
    unsigned i;                                 \
                                                \
    for (i = 1;                                 \
         i < sizeof(data)/sizeof(data[0]);      \
         i++) {                                 \
      result = lqe_step(type, &lqe_param,       \
                        &lqe_state,             \
                        gen_make(type,          \
                                 data[i]));     \
    }                                           \
                                                \
    info(#type ": %f", gen_float(type,          \
                                 result));      \
  }

int main(void) {  
  test_lqe(float);
  test_lqe(fix8);
  test_lqe(fix16);
  test_lqe(fix24);
  
  return 0;
}

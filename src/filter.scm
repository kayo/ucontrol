;;
;; Window functions
;; ( https://en.wikipedia.org/wiki/Window_function)
;;

;; Rectangular window function
(define (window-function-args_rectangular N)
  '())

(define (window-function-call_rectangular n)
  1)

;; Triangular window functions
(define (window-function-args_triangular N)
  (list (/ (- N 1) 2)))

(define (window-function-call_triangular n N-1/2)
  (1- (abs (/ (- n N-1/2) N-1/2))))

(define window-function-args_bartlett
  window-function-args_triangular)

(define window-function-call_bartlett
  window-function-call_triangular)

(define window-function-args_fejer
  window-function-args_triangular)

(define window-function-call_fejer
  window-function-call_triangular)

;; Parzen window function
(define (window-function-args_parzen N)
  (list (/ N 2) (/ N 4)))

(define (window-function-call_parzen n N/2 N/4)
  (if (< n N/4)
      (1- (* 6 (expt (/ n N/2) 2)
             (1- (/ n N/2))))
      (* 2 (expt (1- (/ n N/2)) 3))))

;; Welch window function
(define (window-function-args_welch N)
  (list (/ (- N 1) 2)))

(define (window-function-call_welch n N-1/2)
  (1- (expt (/ (- n N-1/2) N-1/2) 2)))

;; Sine window
(define (window-function-args_sine N)
  (list (/ pi (- N 1))))

(define (window-function-call_sine n pi/N-1)
  (sin (* n pi/N-1)))

;; Hann window function
(define window-function-args_hann
  window-function-args_sine)

(define (window-function-call_hann n pi/N-1)
  (expt (sin (* n pi/N-1)) 2))

;; Generic cosine 5
(define (window-function-args_generic-cosine-5 N)
  (let ([pi2/N-1 (/ (* 2 pi) (- N 1))])
    (list pi2/N-1 (* 2 pi2/N-1) (* 3 pi2/N-1) (* 4 pi2/N-1))))

(define (window-function-call_generic-cosine-5 a0 a1 a2 a3 a4)
  (lambda (n pi2/N-1 pi4/N-1 pi6/N-1 pi8/N-1)
    (+ a0
       (* a1 (cos (* n pi2/N-1)))
       (* a2 (cos (* n pi4/N-1)))
       (* a3 (cos (* n pi6/N-1)))
       (* a4 (cos (* n pi8/N-1))))))

;; Hamming window function
(define window-function-args_hamming
  window-function-args_generic-cosine-5)

(define window-function-call_hamming
  (window-function-call_generic-cosine-5
   25/46 -21/46 0 0 0))

;; Hamming window function (zero-phase version)
(define window-function-args_hamming0
  window-function-args_generic-cosine-5)

(define window-function-call_hamming0
  (window-function-call_generic-cosine-5
   25/46 21/46 0 0 0))

;; Nuttall window function
(define window-function-args_nuttall
  window-function-args_generic-cosine-5)

(define window-function-call_nuttall
  (window-function-call_generic-cosine-5
   0.355768 -0.487396 0.144232 -0.012604 0))

;; Blackman window function
(define window-function-args_blackman
  window-function-args_generic-cosine-5)

(define window-function-call_blackman
  (window-function-call_generic-cosine-5
   7938/18608 -9240/18608 1430/18608 0 0))

;; Blackman-Nuttall window function
(define window-function-args_blackman-nuttall
  window-function-args_generic-cosine-5)

(define window-function-call_blackman-nuttall
  (window-function-call_generic-cosine-5
   0.3635819 -0.4891775 0.1365995 -0.0106411 0))

;; Blackman-Harris window function
(define window-function-args_blackman-harris
  window-function-args_generic-cosine-5)

(define window-function-call_blackman-harris
  (window-function-call_generic-cosine-5
   0.35875 -0.48829 0.14128 -0.01168 0))

;; Flat top window function
(define window-function-args_flat-top
  window-function-args_generic-cosine-5)

(define window-function-call_flat-top
  (window-function-call_generic-cosine-5
   1 -1.93 1.29 0.388 0.028))

;; Rife–Vincent window function
(define window-function-args_rife-vincent2
  window-function-args_generic-cosine-5)

(define window-function-call_rife-vincent2
  (window-function-call_generic-cosine-5
   1 -4/3 1/3 0 0))

;;
;; End of window functions
;;

(define (get-window-function name type)
  (get-function (string-append
                 "window-function-" type "_"
                 name)))

;; Calculate weights
(define (filter-calculate filter-order
                          window-type
                          sample-rate
                          pass-frequency
                          cut-off-frequency)
  (let* ([window-function-call_fn (get-window-function
                                   window-type "call")]
         [window-function-args_fn (get-window-function
                                     window-type "args")]
         [window-function-args (if (procedure? window-function-args_fn)
                                   (apply window-function-args_fn
                                          (list filter-order))
                                   '())]
         [sampling (* 2 pi (/ (+ pass-frequency cut-off-frequency)
                              (* 2 sample-rate)))]
         [pulse-behavior (map (lambda (index)
                                (let ([ideal-pulse-behavior
                                       (if (= 0 index)
                                           sampling
                                           (/ (sin (* sampling index))
                                              (* pi index)))]
                                      [window-function-value
                                       (apply window-function-call_fn
                                              (cons index window-function-args))])
                                  (* ideal-pulse-behavior window-function-value)))
                              (gen-seq 0 filter-order))]
         [accumulated-weight (apply + pulse-behavior)])
    (map (lambda (weight)
           (/ weight accumulated-weight))
         pulse-behavior)))

(define filter-keys '("class" "type" "name"
                      "order" "window"
                      "sample" "pass" "cut"))

(define filter-touched '())

(define (filter-found?)
  (let* ([key (apply string-append
                     (map get filter-keys))])
    (if (assoc-ref filter-touched key)
        #f
        (let ()
          (set! filter-touched
            (assoc-set! filter-touched key #t))
          #t))))

(define (filter-exist?)
  (and-map exist? filter-keys))

(define (filter-name)
  (string-substitute
   (get "name")
   '("<class>" "<type>" "<order>" "<window>")
   (list (get-class) (get-type) (get "order")
         (string-substitute
          (get "window")
          '("-") '("_")))))

(define (filter-window)
  (let* ([window-type (get "window")]
         [window-function-call (get-window-function
                                window-type "call")])
    (if (procedure? window-function-call)
        window-type
        (error (string-append
                "Unsupported window function: "
                window-type)))))

(define (filter-wrap val)
  (if (exist? "wrap")
      (string-substitute
       (get "wrap")
       '("<type>" "<name>" "<val>")
       (list (get-type) (filter-name) val))
      val))

(define (filter-weights)
  (let ([weights (filter-calculate
                  (get-num "order")
                  (filter-window)
                  (get-num "sample")
                  (get-num "pass")
                  (get-num "cut"))])
    (apply string-append
         (map (lambda (weight)
                (string-append
                 "  "
                 (filter-wrap
                  (number->string weight))
                 ",\n"))
              weights))))

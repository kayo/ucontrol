#include <ctl/type.h>
#include <ctl/util.h>
#include "lookup.h"

#define fixed_trig(q)                                 \
  fix##q fix##q##_sin(fix##q a) {                     \
    char n = 0;                                       \
                                                      \
    if (gen_lt(fix##q, a, gen_0(fix##q))) {           \
      n = 1;                                          \
      a = gen_neg(fix##q, a);                         \
    }                                                 \
                                                      \
    a %= gen_pi_2(fix##q);                            \
                                                      \
    if (gen_lt(fix##q, gen_pi(fix##q), a)) {          \
      n = !n;                                         \
      a = gen_sub(fix##q, a, gen_pi(fix##q));         \
    }                                                 \
                                                      \
    if (gen_lt(fix##q, gen_pi_inv_2(fix##q), a)) {    \
      a = gen_sub(fix##q, gen_pi(fix##q), a);         \
    }                                                 \
                                                      \
    fix##q r = lookup_val(fix##q,                     \
                          fix##q##_sin, a);           \
                                                      \
    return n ? -r : r;                                \
  }                                                   \
                                                      \
  fix##q fix##q##_cos(fix##q a) {                     \
    return fix##q##_sin(fixed_add(a,                  \
                              gen_pi_inv_2(fix##q))); \
  }

fixed_trig(8);
fixed_trig(16);
fixed_trig(24);

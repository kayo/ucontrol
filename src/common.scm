(define pi (* 4 (atan 1)))

;; Generate sequence
(define (gen-seq index limit)
  (if (< index limit)
      (cons index (gen-seq (1+ index) limit))
      '()))

(define (has? key)
  (and (exist? key)
       (not (string=? "" (get key)))))

(define (get-num key . def)
  (let ([defval (if (null? def) #f (car def))])
    (if (exist? key)
        (let* ([lst (stack key)]
               [val (if (null? lst) #f (car lst))]
               [num (if (string? val) (string->number val) #f)]
               [uni (if (null? lst) #f (if (null? (cdr lst)) #f (cadr lst)))])
          (if (number? num)
              (if (string? uni)
                  (cond ((string=? "KHz" uni) (* 1000 num))
                        ((string=? "MHz" uni) (* 1000000 num))
                        ((string=? "GHz" uni) (* 1000000000 num))
                        ((string=? "S" uni) (/ 1 num))
                        ((string=? "mS" uni) (/ 1000 num))
                        ((string=? "uS" uni) (/ 1000000 num))
                        ((string=? "nS" uni) (/ 1000000000 num))
                        (else num))
                  num)
              defval))
        defval)))

(define (and-map fn ls)
  (if (null? ls)
      #t
      (and (apply fn (list (car ls)))
           (and-map fn (cdr ls)))))

(define (get-function name)
  (eval (string->symbol name)
        (interaction-environment)))

(define (get-spec)
  (string-append
   (if (exist? "static") "static " "")
   (if (exist? "const") "const " "")))

(define (get-class)
  (get "class"))

(define (get-type)
  (string-substitute
   (get "type")
   '("<class>")
   (list (get "class"))))

[+ autogen5 template -*- mode: poly-c+autogen; -*-
+][+ (add-to-load-path ".")
+][+ (load "common.scm")
+][+ (load "filter.scm")
+][+ define filter_gen
+][+   if (filter-found?)
+][+     if (filter-exist?)
+]
/* filter weights */
[+ (get-spec) +][+ (get-type) +] [+ (filter-name) +][] = {
[+       (filter-weights)
+]};
[+       else (filter-exist?)
+][+       for filter
+][+         filter_gen
+][+       endfor filter
+][+     endif (filter-exist?)
+][+   endif (filter-found?)
+][+ enddef filter_gen
+][+ filter_gen
+]

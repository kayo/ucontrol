(define lookup-tables '())

(define (lookup-is-new? name)
  (if (assoc-ref lookup-tables name)
      #f
      (let ()
        (set! lookup-tables
          (assoc-set! lookup-tables
                      name #t))
        #t)))

(define lookup-keys
  '("type" "name" "size" "from" "to"))

(define (lookup-found?)
  (and (and-map has? lookup-keys)
       (lookup-is-new? (lookup-name))))

(define (lookup-name)
  (if (exist? "name")
      (string-substitute
       (get "name")
       '("<type>")
       (list (get-type)))))

(define (lookup-wrap val)
  (if (exist? "wrap")
      (string-substitute
       (get "wrap")
       '("<type>" "<arg>")
       (list (get "type") val))
      val))

(define (lookup-func val)
  (if (exist? "func")
      (string-substitute
       (get "func")
       '("<type>" "<arg>")
       (list (get "type") val))        
      val))

(define (lookup-steps)
  (define (gen i n)
    (if (<= i n)
        (cons i
              (gen (+ 1 i) n))
        '()))
  (gen 0 (string->number
          (get "size"))))

(define (lookup-step)
  (string-append
   "(" (get "to") " - "
   (get "from") ") / "
   (get "size") ".0"))

[+ autogen5 template -*- mode: poly-c+autogen; -*-
+][+ (add-to-load-path ".")
+][+ (load "common.scm")
+][+ (load "lookup.scm")
+][+ define lookup_gen
+][+   for lookup
+][+     if (lookup-found?)
+]/*
 * Lookup table `[+ (lookup-name) +]`
 */
#define [+ (lookup-name) +]_from [+ (lookup-wrap (get "from")) +]
#define [+ (lookup-name) +]_step [+ (lookup-wrap (lookup-step)) +]
[+ (get-spec) +][+ (get-type) +] [+ (lookup-name) +]_data[] = {
[+         (apply string-append (map (lambda (step)
             (string-append "  " (lookup-wrap (lookup-func (string-append (number->string step) ".0 * " (lookup-step) " + " (get "from")))) ",\n"))
             (lookup-steps)))
+]};
[+       else (lookup-found?)
+][+       lookup_gen
+][+     endif (lookup-found?)
+][+   endfor lookup
+][+ enddef lookup_gen
+][+ lookup_gen
+]

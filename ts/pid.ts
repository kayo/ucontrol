/**
 * @module pid PID controller
 *
 * This module implements proportional integral derivative (PID) controller
 */

import { clamp_val } from "./util";

/**
 * PID parameters type
 */
export interface pid_param_t {
    /**
     * Proportional factor (Kp)
     */
    Kp: number;
    /**
     * Integral factor (1/Ti)
     */
    Ki: number;
    /**
     * Minimum integral error limit
     */
    Ei_min: number;
    /**
     * Maximum integral error limit
     */
    Ei_max: number;
    /**
     * Derivative factor (1/Kd)
     */
    Td: number;
}

/**
 * Create new PID params
 */
export function pid_param(): pid_param_t {
    return { Kp: 0, Ki: 0, Ei_min: 0, Ei_max: 0, Td: 0 };
}

/**
 * PID state type
 */
export interface pid_state_t {
    /**
     * Last error
     *
     * The difference between target and actual values.
     */
    E: number;
    /**
     * @brief Integral of error
     *
     * The accumulated error.
     */
    Ei: number;
}

/**
 * Create new PID state
 */
export function pid_state(): pid_state_t {
    return { E: 0, Ei: 0 };
}

/**
 * Initialize PID state with zeros
 *
 * @param[in,out] state The pointer to PID state
 */
export function pid_init(state: pid_state_t): void {
    state.E = 0;
    state.Ei = 0;
}

/**
 * Set proportional factor value
 *
 * @param[in,out] param The pointer to PID parameters
 * @param[in] proportional The proportional factor value
 */
export function pid_set_Kp(param: pid_param_t, proportional: number): void {
    param.Kp = proportional;
}

/**
 * Set integral factor value
 *
 * @param[in,out] param The pointer to PID parameters
 * @param[in] integral The integral factor value
 *
 * The value of integral factor must be represented in time units.
 * The integral factor value depend from sampling time.
 */
export function pid_set_i(param: pid_param_t, integral: number): void {
    param.Ki = 1 / integral;
}

/**
 * Set integral factor value
 *
 * @param[in,out] param The pointer to PID parameters
 * @param[in] integral The integral factor time
 * @param[in] period The sampling time (control step period)
 *
 * The value of integral factor must be represented in time units.
 */
export function pid_set_Ti(param: pid_param_t, integral: number, period: number): void {
    pid_set_i(param, integral * period);
}

/**
 * Limiting integral error to the desired range
 *
 * @param[in,out] param The pointer to PID parameters
 * @param[in] min The minimum of integral error
 * @param[in] max The maximum of integral error
 */
export function pid_set_Ei_range(param: pid_param_t, min: number, max: number): void {
    param.Ei_min = min;
    param.Ei_max = max;
}

/**
 * Limiting integral error to the absolute maximum
 *
 * @param[in,out] param The pointer to PID parameters
 * @param[in] max The absolute maximum of integral error
 */
export function pid_set_Ei_limit(param: pid_param_t, lim: number): void {
    pid_set_Ei_range(param, -lim, lim);
}

/**
 * Set derivative factor value
 *
 * @param[in,out] param The pointer to PID parameters
 * @param[in] derivative The derivative factor value
 *
 * The value of derivative factor must be represented in time units.
 * The derivative factor value depend from sampling time.
 */
export function pid_set_d(param: pid_param_t, derivative: number): void {
    param.Td = derivative;
}

/**
 * Set derivative factor value
 *
 * @param[in,out] param The pointer to PID parameters
 * @param[in] derivative The derivative factor value
 * @param[in] period The sampling time (control step period)
 *
 * The value of derivative factor must be represented in time units.
 */
export function pid_set_Td(param: pid_param_t, derivative: number, period: number): void {
    pid_set_d(param, derivative * period);
}

/**
 * Evaluate PID control step
 *
 * @param[in] param The pointer to PID parameters
 * @param[in,out] state The pointer to PID state
 * @param[in] error The actual error value
 * @return The result control value
 */
export function pid_step(param: pid_param_t, state: pid_state_t, error: number): number {
    const Ed = error - state.E;
    state.Ei = clamp_val(state.Ei + error, param.Ei_min, param.Ei_max);
    state.E = error;
    return (error + state.Ei * param.Ki + Ed * param.Td) * param.Kp;
}

/**
 * @module ab αβ coordinate transformations
 *
 * This module implements alpha-beta transformations (also known as Clarke transformations).
 */

import { abc_t, ab_t, inv_2, sqrt_3_inv_2, inv_sqrt_3 } from "./type";

/**
 * Convert `ABC` coords to `αβ` coords
 *
 * The direct Clarke transformation
 *
 * @param[in] abc The pointer to `ABC` vector values
 * @param[out] ab The pointer to `αβ` vector values
 */
export function abc_to_ab(abc: abc_t, ab: ab_t): void {
    ab.a = abc.a;
    ab.b = (abc.a + 2 * abc.b) * inv_sqrt_3;
}

/**
 * Convert `αβ` coords to `ABC` coords
 *
 * The inverted Clarke transformation
 *
 * @param[in] ab The pointer to `αβ` vector values
 * @param[out] abc The pointer to `ABC` vector values
 */
export function ab_to_abc(ab: ab_t, abc: abc_t): void {
    const half_a = ab.a * inv_2;
    const half_sqrt_3_b = ab.b * sqrt_3_inv_2;

    abc.a = ab.a;
    abc.b = half_sqrt_3_b - half_a;
    abc.c = -half_sqrt_3_b - half_a;
}

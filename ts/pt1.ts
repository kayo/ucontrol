/**
 * @module pt1 PT1 filter
 *
 * This part implements proportional transmission behavior filtering with delay of the first order (PT1).
 */

/**
 * PT1 parameters type
 */
export interface pt1_param_t {
    /**
     * The time factor of filter
     *
     * 1.0 / (1.0 + Tf/Ts)
     */
    Kx: number;
    /**
     * @brief The time factor of filter
     *
     * (Tf/Ts) / (1.0 + Tf/Ts)
     */
    _Kx: number;
}

/**
 * Create filter parameters
 */
export function pt1_param(): pt1_param_t {
    return { Kx: 1, _Kx: 0 };
}

/**
 * Set time factor of filter
 *
 * @param[in,out] param The pointer to filter parameters
 * @param[in] time The time factor value
 * @param[in] period The sampling time (step period)
 */
export function pt1_set_T(param: pt1_param_t, time: number, period: number): void {
    const T = time / period;
    const T1 = T + 1;

    param.Kx = 1 / T1;
    param._Kx = T / T1;
}

/**
 * PT1 state type
 */
export interface pt1_state_t {
    /**
     * The previous filtered value
     */
    X: number;
}

/**
 * Create filter state
 */
export function pt1_state(): pt1_state_t {
    return { X: 0 };
}

/**
 * Initialize filter state
 *
 * @param[in,out] state The pointer to filter state
 * @param[in] value The initial source value
 */
export function pt1_init(state: pt1_state_t, value: number): void {
    state.X = value;
}

/**
 * Evaluate filtering step
 *
 * @param[in] param The pointer to filter parameters
 * @param[in,out] state The pointer to filter state
 * @param[in] value The source value
 * @return The filtered value
 */
export function pt1_step(param: pt1_param_t, state: pt1_state_t, value: number): number {
    return state.X = param.Kx * value + param._Kx * state.X;
}

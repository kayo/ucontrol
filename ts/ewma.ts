/**
 * @module ewma EWMA filter
 *
 * This part implements exponentially weighted moving average (EWMA) filtering.
 */

/**
 * Filter parameters type
 *
 * @return The filter parameters structure
 */
export interface ewma_param_t {
    /**
     * The value of lambda parameter
     */
    L: number;
    /**
     * The value of 1-lambda parameter
     */
    _L: number;
}

/**
 * Create EWMA parameters with default lambda
 */
export function ewma_param(): ewma_param_t {
    return { L: 1, _L: 0 };
}

/**
 * Create EWMA parameters using lambda factor
 *
 * @param[in] lambda The value of lambda factor
 * @return The filter parameters constant
 *
 * Usually the lambda factor can be treated as the weight of actual value in result.
 * This meaning that when the lambda equals to 1 then no smoothing will be applied.
 * The less lambda does more smoothing and vise versa.
 */
export function ewma_make_lambda(lambda: number): ewma_param_t {
    return { L: lambda, _L: 1 - lambda };
}

/**
 * Create EWMA parameters using N factor
 *
 * @param[in] N The value of N factor [1...]
 * @return The filter parameters constant
 *
 * Usually the N factor can be treated as the number of steps for smoothing.
 * This meaning that when the N equals to 1 then no smoothing will be applied.
 * The more N does more smoothing and vise versa.
 */
export function ewma_make_N(N: number): ewma_param_t {
    return ewma_make_lambda(2 / (N + 1));
}

/**
 * Initialize EWMA parameters using lambda factor
 *
 * @param[in,out] param The pointer to filter parameters
 * @param[in] lambda The value of lambda factor
 *
 * Usually the lambda factor can be treated as the weight of actual value in result.
 * This meaning that when the lambda equals to 1 then no smoothing will be applied.
 * The less lambda does more smoothing and vise versa.
 */
export function ewma_set_lambda(param: ewma_param_t, lambda: number): void {
    param.L = lambda;
    param._L = 1 - lambda;
}

/**
 * Initialize EWMA parameters using N factor
 *
 * @param[in,out] param The pointer to filter parameters
 * @param[in] N The value of N factor [1...]
 *
 * Usually the N factor can be treated as the number of steps for smoothing.
 * This meaning that when the N equals to 1 then no smoothing will be applied.
 * The more N does more smoothing and vise versa.
 */
export function ewma_set_N(param: ewma_param_t, N: number): void {
  /* lambda = 2 / (N + 1)
     N = 1 => lambda = 1
     N > 1 => lambda < 1 */
    ewma_set_lambda(param, 2 / (N + 1));
}

/**
 * Initialize EWMA parameters using time factor
 *
 * @param[in,out] param The pointer to filter parameters
 * @param[in] time The smooth time value
 * @param[in] period The sampling time (control step period)
 *
 */
export function ewma_set_T(param: ewma_param_t, time: number, period: number): void {
  /* lambda = 2 / (time / period + 1)
     lambda = (2 * period) / (time + period)
     time = period => lambda = 1
     time > period => lambda < 1 */
    ewma_set_lambda(param, 2 * period / (time + period));
}

/**
 * Filter state type
 */
export interface ewma_state_t {
    /**
     * The last value
     */
    X: number;
}

/**
 * Create filter state
 */
export function ewma_state(): ewma_state_t {
    return { X: 0 };
}

/**
 * Initialize filter state
 *
 * @param[in,out] state The pointer to filter state
 * @param[in] value The initial value
 */
export function ewma_init(state: ewma_state_t, value: number): void {
    state.X = value;
}

/**
 * Evaluate filtering step
 *
 * @param[in,out] param The pointer to filter parameters
 * @param[in,out] state The pointer to filter state
 * @param[in] value The source value
 * @return The filtered value
 */
export function ewma_step(param: ewma_param_t, state: ewma_state_t, value: number): number {
    return state.X = param.L * value + param._L * state.X;
}

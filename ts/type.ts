/**
 * @module type Widely used types and constants
 */

const { sqrt, sin, cos, PI } = Math;

export { sin, cos };

/**
 * The half of identity scalar (1/2)
 */
export const inv_2 = 0.5;

/**
 * The square root of three (√3)
 */
export const sqrt_3 = sqrt(3);

/**
 * The half of square root of three (√3/2)
 */
export const sqrt_3_inv_2 = 0.5 * sqrt_3;

/**
 * The inverted square root of three (1/√3)
 */
export const inv_sqrt_3 = 1.0 / sqrt_3;

/**
 * The PI constant (π)
 */
export const pi = PI;

/**
 * The double PI constant (2π)
 */
export const pi_2 = 2 * PI;

/**
 * The half of PI constant (π/2)
 */
export const pi_inv_2 = inv_2 * PI;

/**
 * The αβ vector data
 */
export interface ab_t {
    /**
     * The α component value
     */
    a: number;
    /**
     * The β component value
     */
    b: number;
}

/**
 * Create new αβ vector
 */
export function ab(): ab_t {
    return { a: 0, b: 0 };
}

/**
 * The @p ABC vector data
 */
export interface abc_t {
    /**
     * The A component value
     */
    a: number;
    /**
     * The B component value
     */
    b: number;
    /**
     * The C component value
     */
    c: number;
}

/**
 * Create new ABC vector
 */
export function abc(): abc_t {
    return { a: 0, b: 0, c: 0 };
}

/**
 * The DQ vector data
 */
export interface dq_t {
    /**
     * The d component value
     */
    d: number;
    /**
     * The q component value
     */
    q: number;
}

/**
 * Create new DQ vector
 */
export function dq(): dq_t {
    return { d: 0, q: 0 };
}

/**
 * @module lqe LQE (Kalman) filter
 *
 * This module implements linear quadratic estimation (LQE) filtering which also known as Kalman filter.
 */

/**
 * Filter parameters type
 */
export interface lqe_param_t {
    /**
     * The factor of actual value to previous actual value
     */
    F: number;
    /**
     * The factor of measured value to actual value
     */
    H: number;
    /**
     * The measurement noise
     */
    Q: number;
    /**
     * The environment noise
     */
    R: number;
}

/**
 * Create filter parameters
 */
export function lqe_param(): lqe_param_t {
    return { F: 1, H: 1, Q: 0, R: 0 };
}

/**
 * Set factor of actual value to previous actual value
 *
 * @param[in,out] param The pointer to filter parameters
 * @param[in] f The factor value
 */
export function lqe_set_F(param: lqe_param_t, f: number): void {
    param.F = f;
}

/**
 * Set factor of actual value to previous actual value
 *
 * @param[in,out] param The pointer to filter parameters
 * @param[in] f The factor value
 * @param[in] period The sampling time (control step period)
 */
export function lqe_set_F_period(param: lqe_param_t, f: number, period: number): void {
    lqe_set_F(param, f * period);
}

/**
 * Set factor of measured value to actual value
 *
 * @param[in,out] param The pointer to filter parameters
 * @param[in] h The factor value
 */
export function lqe_set_H(param: lqe_param_t, h: number): void {
    param.H = h;
}

/**
 * Set measurement noise
 *
 * @param[in,out] param The pointer to filter parameters
 * @param[in] q The factor value
 */
export function lqe_set_Q(param: lqe_param_t, q: number): void {
    param.Q = q;
}

/**
 * Set environment noise
 *
 * @param[in,out] param The pointer to filter parameters
 * @param[in] r The factor value
 */
export function lqe_set_R(param: lqe_param_t, r: number): void {
    param.R = r;
}

/**
 * Filter state type
 */
export interface lqe_state_t {
    /**
     * The state
     */
    X: number;
    /**
     * The covariance
     */
    P: number;
}

/**
 * Create filter state
 */
export function lqe_state(): lqe_state_t {
    return { X: 0, P: 0 };
}

/**
 * Initialize filter state
 *
 * @param[in,out] state The pointer to filter state
 * @param[in] x The initial source value
 * @param[in] p The initial covariance
 */
export function lqe_init(state: lqe_state_t, x: number, p: number): void {
    state.P = p;
    state.X = x;
}

/**
 * Evaluate filtering step
 *
 * @param[in] param The pointer to filter parameters
 * @param[in,out] state The pointer to filter state
 * @param[in] x The source value
 * @return The filtered value
 */
export function lqe_step(param: lqe_param_t, state: lqe_state_t, x: number): number {
    /* prediction: */
    /* predicted state */
    const X0 = param.F * state.X;
    /* predicted covariance */
    const P0 = param.F * param.F * state.P + param.Q;
    /* correction: */
    const K = param.H * P0 / (param.H * param.H * P0 + param.R);

    state.P = (1 - K * param.H) * P0;
    return state.X = X0 + K * (x - param.H * X0);
}

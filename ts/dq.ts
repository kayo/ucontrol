/**
 * @module dq DQ coordinate transformations
 *
 * This module implements direct-quadrature-zero transformations (also known as Park transformations).
 */

import { ab_t, dq_t, sin, cos } from "./type";

/**
 * Convert stationary `αβ` coordinates to rotating `DQ` coordinates
 *
 * The direct Park transformation
 *
 * @param[in] ab The pointer to stationary `αβ` vector values
 * @param[in] phy The rotating angle value
 * @param[out] dq The pointer to rotating `DQ` vector values
 */
export function ab_to_dq(ab: ab_t, phy: number, dq: dq_t): void {
    const sin_phy = sin(phy);
    const cos_phy = cos(phy);

    dq.d = ab.a * cos_phy + ab.b * sin_phy;
    dq.q = ab.b * cos_phy - ab.a * sin_phy;
}

/**
 * Convert rotating `DQ` coordinates to stationary `αβ` coordinates
 *
 * The inverted Park transformation
 *
 * @param[in] dq The pointer to rotating `DQ` vector values
 * @param[in] phy The rotating angle value
 * @param[out] ab The pointer to stationary `αβ` vector values
 */
export function dq_to_ab(dq: dq_t, phy: number, ab: ab_t): void {
    const sin_phy = sin(phy);
    const cos_phy = cos(phy);

    ab.a = dq.d * cos_phy - dq.q * sin_phy;
    ab.b = dq.q * cos_phy + dq.d * sin_phy;
}

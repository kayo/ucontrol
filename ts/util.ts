/**
 * @module util Utilities
 *
 * This module implements some useful utilities.
 */

import { abc_t, inv_2, pi_2 } from "./type";

const { abs, min, max } = Math;

/**
 * Get absolute value
 *
 * @param[in] value The actual value
 * @return The absolute value
 */
export { abs as abs_val };

/**
 * Estimate actual error using target and actual value
 *
 * @param[in] target The target value
 * @param[in] actual The actual value
 * @return error The error value
 */
export function error_val(target: number, actual: number): number {
    return actual - target;
}

/**
 * Get minimum value of two values
 *
 * @param[in] val1 The first value
 * @param[in] val2 The second value
 * @return The result value
 *
 * If required the both values must be prescaled to common range.
 *
 * res = val1 < val2 ? val1 : val2
 */
export { min as min_val };

/**
 * Get maximum value of two values
 *
 * @param[in] val1 The first value
 * @param[in] val2 The second value
 * @return The result value
 *
 * If required the both values must be prescaled to common range.
 *
 * res = val1 < val2 ? val2 : val1
 */
export { max as max_val };

/**
 * Get middle value between two values
 *
 * @param[in] val1 The first value
 * @param[in] val2 The second value
 * @return The result value
 *
 * If required the both values must be prescaled to common range.
 *
 * res = (val1 + val2) / 2
 */
export function mid_val(val1: number, val2: number): number {
    return lerp_val(val1, val2, inv_2);
}

/**
 * Linear interpolation between two values
 *
 * @param[in] val1 The first value
 * @param[in] val2 The second value
 * @param[in] fact The interpolation factor [0...1]
 * @return The result value
 *
 * If required the both values must be prescaled to common range.
 *
 * res = val1 * (1 - fact) + val2 * fact
 */
export function lerp_val(val1: number, val2: number, fact: number): number {
    return val1 * (1.0 - fact) + val2 * fact;
}

/**
 * @brief Reduce value to range
 *
 * @param[in] type The type of values
 * @param[in] val The source value
 * @param[in] min The minimum value
 * @param[in] max The maximum value
 * @return The result value
 *
 * res = val < min ? min : max < val ? max : val
 */
export function clamp_val(val: number, min: number, max: number): number {
    return val >= min ? val <= max ? val : max : min;
}

/**
 * @brief Re-scale value from range [a, b] to range [l, h]
 *
 * @param[in] type The type of values
 * @param[in] val The source value
 * @param[in] a The minimum value of source range
 * @param[in] b The maximum value of source range
 * @param[in] l The minimum value of target range
 * @param[in] h The maximum value of target range
 * @return The result value
 *
 * res = l + (val - a) * (h - l) / (b - a)
 */
export function scale_val(val: number, a: number, b: number, l: number, h: number): number {
    return l + (val - a) * (h - l) / (b - a);
}

/**
 * Scale ABC coords from range [f, t] to range [l, h]
 *
 * @param[in] abc The source value
 * @param[in] f The minimum value of source range
 * @param[in] t The maximum value of source range
 * @param[in] l The minimum value of target range
 * @param[in] h The maximum value of target range
 * @param[out] Sabc The result value
 */
export function scale_abc(abc: abc_t, f: number, t: number, l: number, h: number, abcs: abc_t): void {
    abcs.a = scale_val(abc.a, f, t, l, h);
    abcs.b = scale_val(abc.b, f, t, l, h);
    abcs.c = scale_val(abc.c, f, t, l, h);
}

/**
 * Calculate magnitude-frequency modulator step
 *
 * @param[in] modulation_period The period of modulation step
 * @param[in] field_frequency The frequency of field rotation
 * @return The step of modulation angle
 *
 * The value is dependent from modulation period and field revolution speed.
 */
export function mf_step(modulation_period: number, field_frequency: number): number {
    return pi_2 * modulation_period * field_frequency;
}
